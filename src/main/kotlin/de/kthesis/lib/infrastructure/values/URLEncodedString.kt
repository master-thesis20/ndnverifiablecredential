package de.kthesis.lib.infrastructure.values

import java.net.URLEncoder

/**
 *  "safe" encode string for url ( lead to problems, when parameter string in Interest names where not safe encoded)
 */
data class URLEncodedString(var string: String) {

    init {
        string = URLEncoder.encode(string, Charsets.UTF_8)
    }
}