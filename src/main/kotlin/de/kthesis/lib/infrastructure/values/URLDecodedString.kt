package de.kthesis.lib.infrastructure.values

import java.net.URLDecoder

data class URLDecodedString(var string: String) {

    init {
        string = URLDecoder.decode(string, Charsets.UTF_8)
    }

}