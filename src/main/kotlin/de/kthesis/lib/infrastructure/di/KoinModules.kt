package de.kthesis.lib.infrastructure.di

import de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure.CertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure.DataRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure.PendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { DataRepository() }
    single<IDataRepository> { DataRepository() }
    single<ICertificateRepository> { CertificateRepository() }
    single<IPendingRegistrationRepository> { PendingRegistrationRepository() }
}