package de.kthesis.lib.infrastructure.utils.cryptography

import de.kthesis.lib.infrastructure.utils.components
import net.named_data.jndn.ContentType
import net.named_data.jndn.Data
import net.named_data.jndn.Interest
import net.named_data.jndn.KeyLocator
import net.named_data.jndn.KeyLocatorType
import net.named_data.jndn.Name
import net.named_data.jndn.Sha256WithEcdsaSignature
import net.named_data.jndn.Sha256WithRsaSignature
import net.named_data.jndn.encoding.WireFormat
import net.named_data.jndn.security.DigestAlgorithm
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.KeyType
import net.named_data.jndn.security.ValidityPeriod
import net.named_data.jndn.security.certificate.PublicKey
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.util.Blob
import net.named_data.jndn.util.Common

/**
 * Important utility class for signing Interests and Data and creating Certificates.
 *
 * Since the methods of [KeyChain] not allow to set the keylocator field custom, this methods are needed.
 */
object CertificateUtil {

    /**
     * Sign the given interest with the private key and set a custom key name for the keylocator field.
     *
     * @param keyChain The keyChain which contains the private key associated with [signingKeyName]
     * @param interest The Interest which should be signed (signature will be append to name)
     * @param keylocatorKeyName The key name which should be set for the keylocator field
     * @param signingKeyName The name of the actual signing key (must be in the keychain available)
     * @param validNotBefore The start time when the signature becomes valid (default is now)
     * @param validNotAfter The end for the signature validation period (default is now+1 year)
     */
    fun sign(
        keyChain: KeyChain,
        interest: Interest,
        keylocatorKeyName: Name,
        signingKeyName : Name,
        validNotBefore: Double = Common.getNowMilliseconds(),
        // default a year valid
        validNotAfter: Double = validNotBefore + (356.0 * 24 * 60 * 60 * 1000)
    ): Interest {
        // public key which is associated to the private key
        val associatedPublicKey = PublicKey(keyChain.tpm.getPublicKey(signingKeyName))


        // get the proper signature for the key
        val signatureInfo = if (associatedPublicKey.keyType == KeyType.RSA) {
            Sha256WithRsaSignature()
        } else {
            if (associatedPublicKey.keyType != KeyType.EC) {
                throw AssertionError("Unsupported key type")
            }
            Sha256WithEcdsaSignature()
        }

        // set the signature information and the validity period
        KeyLocator.getFromSignature(signatureInfo).type = KeyLocatorType.KEYNAME
        KeyLocator.getFromSignature(signatureInfo).keyName = keylocatorKeyName
        ValidityPeriod.getFromSignature(signatureInfo).setPeriod(validNotBefore, validNotAfter)

        interest.name.append(WireFormat.getDefaultWireFormat().encodeSignatureInfo(signatureInfo))
        interest.name.append(Name.Component())
        // create the signature
        val encoding = interest.wireEncode()

        val signatureBytes =
            keyChain.tpm.sign(encoding.signedBuf(), signingKeyName, DigestAlgorithm.SHA256)
        signatureInfo.signature = signatureBytes

        val encodedSignature = WireFormat.getDefaultWireFormat().encodeSignatureValue(signatureInfo)

        interest.name = interest.name.getPrefix(-1).append(encodedSignature)

        return interest
    }

    /**
     * Sign the given interest with the private key and set a custom key name for the keylocator field.
     *
     * @param keyChain The keyChain which contains the private key associated with [signingKeyName]
     * @param data The Data which will be signed.
     * @param keylocatorKeyName The key name which should be set for the keylocator field
     * @param signingKeyName The name of the actual signing key (must be in the keychain available)
     * @param validNotBefore The start time when the signature becomes valid (default is now)
     * @param validNotAfter The end for the signature validation period (default is now+1 year)
     */
    fun sign(
        keyChain: KeyChain,
        data: Data,
        keylocatorKeyName: Name,
        signingKeyName : Name,
        validNotBefore: Double = Common.getNowMilliseconds(),
        // default a year valid
        validNotAfter: Double = validNotBefore + (356.0 * 24 * 60 * 60 * 1000)
    ): Data {
        // public key which is associated to the private key
        val associatedPublicKey = PublicKey(keyChain.tpm.getPublicKey(signingKeyName))

        // get the proper signature for the key
        if (associatedPublicKey.keyType == KeyType.RSA) {
            data.signature = Sha256WithRsaSignature()
        } else {
            if (associatedPublicKey.keyType != KeyType.EC) {
                throw AssertionError("Unsupported key type")
            }
            data.signature = Sha256WithEcdsaSignature()
        }

        val signatureInfo = data.signature
        // set the signature information and the validity period
        KeyLocator.getFromSignature(signatureInfo).type = KeyLocatorType.KEYNAME
        KeyLocator.getFromSignature(signatureInfo).keyName = keylocatorKeyName
        ValidityPeriod.getFromSignature(signatureInfo).setPeriod(validNotBefore, validNotAfter)

        // create the signature
        val encoding = data.wireEncode()
        val signatureBytes =
            keyChain.tpm.sign(encoding.signedBuf(), signingKeyName, DigestAlgorithm.SHA256)
        signatureInfo.signature = signatureBytes


        // update the encoding information
        data.wireEncode()

        return data
    }

    /**
     * Create a certificate containing the public key and signs it.
     *
     * @param caPublicKeyName The name of the CA key, which should be used to sign the certificate
     * @param keyChain The Keychain of the CA containing a keypair with the name [caPublicKeyName]
     * @param identity The prefix for the certificate, e.g. "/faber"
     * @param publicKey The public key which should be signed.
     * @param validNotBefore The start time when the certificate becomes valid (default is now)
     * @param validNotAfter The end for the certificate validation period (default is now+1 year)
     */
    fun createCertificateV2(
        identity: Name,
        caPublicKeyName: Name,
        publicKey: PublicKey,
        keyChain: KeyChain,
        validNotBefore: Double = Common.getNowMilliseconds(),
        // default a year valid
        validNotAfter: Double = validNotBefore + (356.0 * 24 * 60 * 60 * 1000)
    ): CertificateV2 {
        val certificate = CertificateV2().apply {
            // /KEY/[KeyId]/[IssuerId]/[Version]
            name = Name(identity).append(Name("/KEY/${caPublicKeyName.components().last()}/CA/"))
                .appendVersion(validNotBefore.toLong())
            metaInfo.type = ContentType.KEY
            // fresh as long as it is valid
            metaInfo.freshnessPeriod = validNotAfter - validNotBefore
            // only one packet
            metaInfo.finalBlockId = Name.Component("0")
        }

        certificate.content = Blob(publicKey.keyDer.immutableArray)

        // get the proper signature for the key
        if (publicKey.keyType == KeyType.RSA) {
            certificate.signature = Sha256WithRsaSignature()
        } else {
            if (publicKey.keyType != KeyType.EC) {
                throw AssertionError("Unsupported key type")
            }
            certificate.signature = Sha256WithEcdsaSignature()
        }

        // set the signature information and the validity period
        val signatureInfo = certificate.signature
        KeyLocator.getFromSignature(signatureInfo).type = KeyLocatorType.KEYNAME
        KeyLocator.getFromSignature(signatureInfo).keyName = caPublicKeyName
        ValidityPeriod.getFromSignature(signatureInfo).setPeriod(validNotBefore, validNotAfter)

        // create the signature
        val encoding = certificate.wireEncode()
        val signatureBytes =
            keyChain.tpm.sign(encoding.signedBuf(), caPublicKeyName, DigestAlgorithm.SHA256)
        signatureInfo.signature = signatureBytes

        // update the encoding information
        certificate.wireEncode()
        return certificate
    }

    /**
     * Create a self-signed certificate containing the public key and signs it.
     *
     * @param caPublicKeyName The name of the CA key, which should be used to sign the certificate
     * @param keyChain The Keychain of the CA containing a keypair with the name [caPublicKeyName]
     * @param keyName The prefix for the certificate, e.g. "/faber"
     * @param publicKey The public key which should be signed.
     * @param validNotBefore The start time when the certificate becomes valid (default is now)
     * @param validNotAfter The end for the certificate validation period (default is now+1 year)
     */
    fun createCertificateV2(
        keyName: Name,
        caPublicKeyName: Name,
        publicKey: PublicKey,
        keyChain: KeyChain,
        privateKeyName : Name,
        validNotBefore: Double = Common.getNowMilliseconds(),
        // default a year valid
        validNotAfter: Double = validNotBefore + (356.0 * 24 * 60 * 60 * 1000)
    ): CertificateV2 {
        val certificate = CertificateV2().apply {
            // /KEY/[KeyId]/[IssuerId]/[Version]
            name = Name(keyName).append(Name("SELF"))
                .appendVersion(validNotBefore.toLong())
            metaInfo.type = ContentType.KEY
            // fresh as long as it is valid
            metaInfo.freshnessPeriod = validNotAfter - validNotBefore
            // only one packet
            metaInfo.finalBlockId = Name.Component("0")
        }

        certificate.content = Blob(publicKey.keyDer.immutableArray)

        // get the proper signature for the key
        if (publicKey.keyType == KeyType.RSA) {
            certificate.signature = Sha256WithRsaSignature()
        } else {
            if (publicKey.keyType != KeyType.EC) {
                throw AssertionError("Unsupported key type")
            }
            certificate.signature = Sha256WithEcdsaSignature()
        }

        // set the signature information and the validity period
        val signatureInfo = certificate.signature
        KeyLocator.getFromSignature(signatureInfo).type = KeyLocatorType.KEYNAME
        KeyLocator.getFromSignature(signatureInfo).keyName = caPublicKeyName
        ValidityPeriod.getFromSignature(signatureInfo).setPeriod(validNotBefore, validNotAfter)

        // create the signature
        val encoding = certificate.wireEncode()
        val signatureBytes =
            keyChain.tpm.sign(encoding.signedBuf(), privateKeyName, DigestAlgorithm.SHA256)
        signatureInfo.signature = signatureBytes

        // update the encoding information
        certificate.wireEncode()
        return certificate
    }
}