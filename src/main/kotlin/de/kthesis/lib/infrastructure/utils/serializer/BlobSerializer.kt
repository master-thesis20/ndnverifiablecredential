package de.kthesis.lib.infrastructure.utils.serializer

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.named_data.jndn.util.Blob
import net.named_data.jndn.util.Common

/**
 * Used to serialize object to string
 */
object BlobSerializer : KSerializer<Blob> {

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("Blob", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Blob = Blob(
        Common.base64Decode((decoder.decodeString()))
    )


    override fun serialize(encoder: Encoder, value: Blob) =
        encoder.encodeString(Common.base64Encode(value.immutableArray))

}