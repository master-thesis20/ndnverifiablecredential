package de.kthesis.lib.infrastructure.utils.serializer

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ForwardingHint
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
/**
 * Used to serialize object to string
 */
object ForwardingHintSerializer : KSerializer<ForwardingHint> {

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("ForwardingHint", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): ForwardingHint = ForwardingHint(
        ListSerializer(NameSerializer).deserialize(decoder)
    )

    override fun serialize(encoder: Encoder, value: ForwardingHint) {
        ListSerializer(NameSerializer).serialize(encoder,value.forwardNames)
    }


}