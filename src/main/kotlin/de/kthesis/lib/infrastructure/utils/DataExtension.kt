package de.kthesis.lib.infrastructure.utils

import de.kthesis.lib.domain.named_verifiable_object.interfaces.IDecryption
import de.kthesis.lib.domain.named_verifiable_object.interfaces.IEncryption
import net.named_data.jndn.Data
import net.named_data.jndn.util.Blob
import java.nio.ByteBuffer
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

/*
Contains methods for en/decryption (currently unused)
 */


fun Data.encryptContent(encryption : IEncryption) {
    content = Blob(encryption.encrypt(content.immutableArray))
}

fun Data.decryptContent(decryption : IDecryption) = Blob(decryption.decrypt(content.immutableArray))

/**
 * AES encrypt the current content with the given key and parameter spec and replace it
 */
fun Data.encryptContent(key : SecretKey, iv : IvParameterSpec) {
    val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cipher.init(Cipher.ENCRYPT_MODE, key,iv)
    content = Blob(cipher.doFinal(content.immutableArray))
}

/**
 * Decrypt the AES encrypted content and return the encrypted content
 */
fun Data.decryptContent(key : SecretKey, iv : IvParameterSpec) : Blob {
    val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cipher.init(Cipher.DECRYPT_MODE, key,iv)
    return Blob(cipher.doFinal(content.immutableArray))
}

fun ByteBuffer.toArr(): ByteArray {
    val arr = ByteArray(this.remaining())
    this.get(arr)
    return arr
}