package de.kthesis.lib.infrastructure.utils

import net.named_data.jndn.Data
import net.named_data.jndn.Interest
import net.named_data.jndn.security.v2.ValidationError
import net.named_data.jndn.security.v2.Validator
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * @return a validation error or null if Interest could be verified.
 */
suspend fun Validator.validate(
    interest: Interest,
) = suspendCoroutine<ValidationError?> { continuation ->
    this.validate(interest, {
        continuation.resume(null)
    }, { _, validationError ->
        continuation.resume(validationError)
    })
}

/**
 * @return a validation error or null if Data could be verified.
 */
suspend fun Validator.validate(
    data: Data,
) = suspendCoroutine<ValidationError?> { continuation ->
    this.validate(data, {
        continuation.resume(null)
    }, { _, validationError ->
        continuation.resume(validationError)
    })
}