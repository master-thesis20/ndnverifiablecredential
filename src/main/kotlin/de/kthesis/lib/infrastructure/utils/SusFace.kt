package de.kthesis.lib.infrastructure.utils

import net.named_data.jndn.Data
import net.named_data.jndn.Face
import net.named_data.jndn.Interest
import java.util.logging.Logger
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

// mostly used for demo
suspend fun Face.expressInterest(interest: Interest) = suspendCoroutine<Data> { continuation ->
    expressInterest(interest, { _, data ->
        continuation.resume(data)
    }) {
        Logger.getLogger(this::class.java.simpleName).warning("Express Interest timeout")
        throw Exception("Timeout")
    }
}