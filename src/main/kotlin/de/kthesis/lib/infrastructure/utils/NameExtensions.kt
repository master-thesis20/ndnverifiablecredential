package de.kthesis.lib.infrastructure.utils

import net.named_data.jndn.Name

/**
 * Split the name into single components
 *
 * e.g. Name("/test/1").components() returns {"test","1"}
 */
fun Name.components() : List<String> {
    return this.toString().split("/").filter { component -> component.isNotBlank() } }

operator fun Name.minus(name : Name) =
   Name(this.toString().removePrefix(name.toString()))

operator fun Name.plus(name:Name) : Name =
    Name(this.toUri()).append(Name(name))


