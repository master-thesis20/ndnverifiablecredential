package de.kthesis.lib.infrastructure.utils.serializer

import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ByteArraySerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.util.Blob

/**
 * Used to serialize object to byte
 */
object CertificateSerializer : KSerializer<CertificateV2> {

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("Certificate", PrimitiveKind.BYTE)

    override fun deserialize(decoder: Decoder): CertificateV2 {
        return CertificateV2().apply {
            wireDecode(Blob(ByteArraySerializer().deserialize(decoder)))
        }
    }

    override fun serialize(encoder: Encoder, value: CertificateV2) {
        encoder.encodeSerializableValue(
            ByteArraySerializer(),
            value.wireEncode().immutableArray
        )
    }

}