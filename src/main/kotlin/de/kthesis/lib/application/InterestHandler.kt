package de.kthesis.lib.application

import de.kthesis.lib.domain.named_verifiable_data_registry.CommandVerificationConfig
import de.kthesis.lib.domain.named_verifiable_data_registry.NamedVerifiableDataRegistry
import de.kthesis.lib.domain.named_verifiable_data_registry.command.CommandType
import de.kthesis.lib.domain.named_verifiable_data_registry.command.DeRegisterCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.DeleteCheckCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.DeleteCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.DownloadCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.InsertCheckCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.InsertCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.NewCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.ProbeCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.RegistryCommandInterest
import de.kthesis.lib.domain.named_verifiable_data_registry.command.SelectCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.StatusCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.command.ValidateCommand
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.service.IDataManagementService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.named_data.jndn.Face
import net.named_data.jndn.Interest
import net.named_data.jndn.security.v2.Validator
import java.util.logging.Logger


/**
 * Communication "heart" of the [NamedVerifiableDataRegistry].
 *
 * Receive Interests and execute the corresponding actions.
 */
class InterestHandler(
    private val repositoryConfig: RepositoryConfig,
    private val registrationConfig: CommandVerificationConfig,
    private val certificateRepository: ICertificateRepository,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
    private var validator: Validator,
    private val dataManagementService: IDataManagementService,
    private val logger: Logger,
    private val face: Face,
    private val dataProvider: DataProvider,
    private val dataRepository: IDataRepository,
) {

    /**
     * Handle the given Interest by executing the given NVDR Interest Commands or return data.
     */
    fun handleInterest(receivedInterest: Interest, face: Face?) {
        logger.info("Handle received Interest")
        // reload config each time in case something changed
        registrationConfig.loadConfig(
            certificateRepository.findAll(),
            pendingRegistrationRepository.findAll()
        )

        // parse the command if the Interest is a NVDR Interest
        val command = RegistryCommandInterest.decodeCommand(
            repositoryConfig.repositoryPrefix,
            interest = receivedInterest
            // if the Interest is not a nvdr command interest, handle it like a normal data interest
            // workaround to let data provider handle interest for certificates
        ) ?: return dataProvider.onInterest(null, receivedInterest, face, -1, null)

        logger.info("Received Interest has command '${command}'")

        // cast the Interest to a NVDR Interest
        val registryCommandInterest = RegistryCommandInterest(
            repositoryConfig.repositoryPrefix,
            command,
            interest = receivedInterest
        )

        CoroutineScope(Dispatchers.IO).launch {
            // execute the command and return execution result
            val commandResponse = execute(registryCommandInterest)

            logger.info("Send response for Interest \"${registryCommandInterest.simpleName}/...\"")

            // sign the response
            repositoryConfig.repositoryKeyChain.sign(commandResponse)

            // send the data pack to the consumer
            face?.putData(commandResponse)
        }
    }

    /**
     * Receive the command interest and execute the corresponding action
     *
     * @param registryCommandInterest The NVDR Command Interest. Refer to the thesis for all command Interests.
     * @return The response data packet
     */
    private suspend fun execute(registryCommandInterest: RegistryCommandInterest): RegistryCommandResponseData =
        when (registryCommandInterest.commandType) {
            CommandType.INSERT -> InsertCommand(
                registryCommandInterest = registryCommandInterest,
                dataManagementService = dataManagementService,
                validator = validator
            )
            CommandType.DELETE -> DeleteCommand(
                registryCommandInterest = registryCommandInterest,
                dataManagementService = dataManagementService,
                validator = validator
            )
            CommandType.INSERT_CHECK -> InsertCheckCommand(
                registryCommandInterest = registryCommandInterest,
                dataManagementService = dataManagementService,
                dataRepository = dataRepository,
                validator = validator
            )
            CommandType.DELETE_CHECK -> DeleteCheckCommand(
                registryCommandInterest = registryCommandInterest,
                dataManagementService = dataManagementService,
                dataRepository = dataRepository,
                validator = validator,
            )
            CommandType.DEREGISTER -> DeRegisterCommand(
                registryCommandInterest = registryCommandInterest,
                certificateRepository = certificateRepository,
                pendingRegistrationRepository = pendingRegistrationRepository,
                validator = validator
            )
            CommandType.NEW -> NewCommand(
                registryCommandInterest = registryCommandInterest,
                pendingRegistrationRepository = pendingRegistrationRepository,
                validator = validator
            )
            CommandType.PROBE -> ProbeCommand(
                registryCommandInterest = registryCommandInterest,
                pendingRegistrationRepository = pendingRegistrationRepository,
                certificateRepository = certificateRepository,
                validator = validator
            )
            CommandType.SELECT -> SelectCommand(
                registryCommandInterest = registryCommandInterest,
                pendingRegistrationRepository = pendingRegistrationRepository,
                validator = validator
            )
            CommandType.STATUS -> StatusCommand(registryCommandInterest, validator)
            CommandType.VALIDATE -> ValidateCommand(
                registryCommandInterest = registryCommandInterest,
                pendingRegistrationRepository = pendingRegistrationRepository,
                certificateRepository = certificateRepository,
                repositoryPrefix = repositoryConfig.repositoryPrefix,
                repositoryKeyChain = repositoryConfig.repositoryKeyChain,
                dataProvider = dataProvider,
                face = face,
                validator = validator,
            )
            CommandType.DOWNLOAD -> DownloadCommand(
                registryCommandInterest = registryCommandInterest,
                pendingRegistrationRepository = pendingRegistrationRepository,
                validator = validator
            )
        }.execute()

}