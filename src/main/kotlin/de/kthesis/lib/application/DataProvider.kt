package de.kthesis.lib.application

import de.kthesis.lib.domain.named_verifiable_data_registry.NamedVerifiableDataRegistry
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import net.named_data.jndn.Face
import net.named_data.jndn.Interest
import net.named_data.jndn.InterestFilter
import net.named_data.jndn.Name
import net.named_data.jndn.OnInterestCallback
import java.util.logging.Logger

/**
 * Simple class, which receives Interest, requesting [NamedVerifiableDataRegistry] stored data packets and
 * return them
 */
class DataProvider(
    private val dataRepository: IDataRepository,
    private val certificateRepository: ICertificateRepository
) : OnInterestCallback {

    override fun onInterest(
        prefix: Name?,
        interest: Interest?,
        face: Face?,
        p3: Long,
        p4: InterestFilter?
    ) {
        if (interest == null) return
        Logger.getLogger(this::class.simpleName).info("Receive Interest ${interest.name}")
        // return data which matches the name or find the first one with the same prefix
        // NOTE: This needs a serious refactoring. How should the repo satify interests? There has
        // to be some kind of conventions to inform the rpo, how the data should be provided.
        val data = dataRepository.find(interest.name)
            ?: dataRepository.findFirstByPrefix(interest.name)
            ?: certificateRepository.find(interest.name)?.certificate
            ?:return

        face?.putData(data)
    }
}