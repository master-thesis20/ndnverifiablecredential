package de.kthesis.lib.application

import net.named_data.jndn.Name
import net.named_data.jndn.security.KeyChain

// TODO replace with simple value wrapper classes for each parameter
data class RepositoryConfig(val repositoryPrefix : Name, val repositoryKeyChain: KeyChain)
