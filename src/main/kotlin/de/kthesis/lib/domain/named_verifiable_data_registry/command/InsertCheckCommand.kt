package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.CommandResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import de.kthesis.lib.domain.named_verifiable_data_registry.service.IDataManagementService
import net.named_data.jndn.security.v2.Validator

/**
 * Insert Check Command of the NVDR Interest to check inserted data.
 *
 * Check if the insertion of the data is in progress or already done.
 */
class InsertCheckCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val dataManagementService: IDataManagementService,
    private val dataRepository: IDataRepository,
    validator: Validator
) : AbstractCommand(registryCommandInterest, validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        // check if the interest is authorized to perform a data insert check
        if (!authenticate()) return RegistryCommandResponseData(
            registryCommandInterest.name,
            CommandResponse(statusCode = StatusCode.AUTHORIZATION_FAILED)
        )

        // get the command parameter
        val commandParameter = registryCommandInterest.decodeParameter<CommandParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for INSERT CHECK command."
                )
            )

        // check if the data deletion is currently executed
        if (dataManagementService.insertDataInProgress(
                commandParameter.data,
                commandParameter.startBlockId,
                commandParameter.endBlockId
            )
        ) {
            return createResponseData(
                CommandResponse(statusCode = StatusCode.INSERTION_IN_PROGRESS)
            )
        }

        // if not executed, check if the data was already inserted
        dataRepository.find(commandParameter.data) ?: return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.INSERTION_NO_SUCH_PROCESS,
                errorInfo = "Data is not inserted and not intend to do so could be found."
            )
        )

        // return data is already inserted
        return createResponseData(
            CommandResponse(statusCode = StatusCode.INSERTION_DONE)
        )
    }

}