package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

/**
 * Shared interface for each object which could be part of a registry command
 */
@kotlinx.serialization.Serializable
sealed class InterestParameter