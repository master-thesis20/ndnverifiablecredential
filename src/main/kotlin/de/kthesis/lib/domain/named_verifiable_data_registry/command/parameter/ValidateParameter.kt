package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

/**
 * Parameter for the VALIDATE Interest for the registration. Challenge is currently not implemented and just a plain string.
 *
 * See https://named-data.net/wp-content/uploads/2017/04/ndn-0050-1-ndncert.pdf for more information.
 */
@kotlinx.serialization.Serializable
data class ValidateParameter(
    val requestId: String,
    // should later be extended to support more parameter
    val challenge: String
) : InterestParameter()
