package de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure

import de.kthesis.lib.domain.named_verifiable_data_registry.repository.CertificateRepositoryEntry
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import net.named_data.jndn.Name

/**
 * Runtime repository to store certificates. Does not store any information persistent.
 *
 * Just for demo purposes.
 */
class CertificateRepository : Repository<Name, CertificateRepositoryEntry>(),
    ICertificateRepository {

    override fun findAll(): List<CertificateRepositoryEntry> = runtimeStorage.values.toList()

    override fun hasNameSpace(nameSpace: Name): Boolean =
        runtimeStorage.values
            .map { certificateRepositoryEntry -> certificateRepositoryEntry.nameSpace }
            .any { registeredNameSpace ->
                registeredNameSpace.isPrefixOf(nameSpace) || nameSpace.isPrefixOf(
                    registeredNameSpace
                )
            }

    /**
     * Keyname is public key name of cert
     */
    override fun add(value: CertificateRepositoryEntry): Boolean =
        _add(value.certificate.keyName, value)

    override fun update(value: CertificateRepositoryEntry): Boolean =
        _update(value.certificate.keyName, value)

    override fun removeForNameSpace(nameSpace: Name) {
        runtimeStorage
            .filter { entry ->
                entry.value.nameSpace == nameSpace
            }.keys.forEach(runtimeStorage::remove)
    }
}