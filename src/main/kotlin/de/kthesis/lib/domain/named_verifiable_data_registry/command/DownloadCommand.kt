package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.DownloadParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.DownloadResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import net.named_data.jndn.security.v2.Validator

/**
 * Download Command of the NVDR Interest to download registered certificates.
 *
 * Last Interest of the registration process.
 */
class DownloadCommand(
    val registryCommandInterest: RegistryCommandInterest,
    val pendingRegistrationRepository: IPendingRegistrationRepository,
    validator: Validator) :
    AbstractCommand(registryCommandInterest, validator) {

    override suspend fun authenticate(): Boolean {
        // download is just a simple data request, since the certificate should be public available
        return true
    }

    override suspend fun execute(): RegistryCommandResponseData {
        if (!authenticate()) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.AUTHORIZATION_FAILED,
                "DOWNLOAD Command Interest could not be authenticated."
            )
        )

        val downloadParameter = registryCommandInterest.decodeParameter<DownloadParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for DOWNLOAD command."
                )
            )

        val entry = pendingRegistrationRepository.find(downloadParameter.requestId)?: return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.NO_CERTIFICATE,
                errorInfo = "Could not parse parameter for DOWNLOAD command."
            )
        )

        // return the certificate as payload
        return createResponseData(
            DownloadResponse(
                certificateV2 = entry.certificate
            )
        )
    }
}