package de.kthesis.lib.domain.named_verifiable_data_registry.command

enum class CommandType {
    INSERT,
    DELETE,
    INSERT_CHECK,
    DELETE_CHECK,
    NEW,
    PROBE,
    SELECT,
    STATUS,
    VALIDATE,
    DOWNLOAD,
    DEREGISTER
}