package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter


/**
 * Parameter for the de registration of nodes by deleting their certificates.
 */
@kotlinx.serialization.Serializable
object RevokeParameter : InterestParameter()
