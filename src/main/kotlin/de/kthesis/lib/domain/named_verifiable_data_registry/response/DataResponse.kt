package de.kthesis.lib.domain.named_verifiable_data_registry.response

@kotlinx.serialization.Serializable
sealed class DataResponse