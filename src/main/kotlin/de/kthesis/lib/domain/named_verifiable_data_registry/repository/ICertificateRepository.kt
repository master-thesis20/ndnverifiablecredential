package de.kthesis.lib.domain.named_verifiable_data_registry.repository

import net.named_data.jndn.Name
import net.named_data.jndn.security.v2.CertificateV2

data class CertificateRepositoryEntry(
    val nameSpace : Name,
    val certificate : CertificateV2
)

/**
 * Key is the name of the assigned name space and the associated value is the registered certificate
 * which can be used to autorize the data management requests.
 */
interface ICertificateRepository : IRepository<Name, CertificateRepositoryEntry> {
    fun findAll(): List<CertificateRepositoryEntry>

    /**
     * @return true if the name space is already assigned
     */
    fun hasNameSpace(nameSpace : Name) :  Boolean
    fun removeForNameSpace(nameSpace: Name)
}