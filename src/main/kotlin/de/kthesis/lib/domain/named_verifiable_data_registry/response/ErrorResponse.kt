package de.kthesis.lib.domain.named_verifiable_data_registry.response

@kotlinx.serialization.Serializable
data class ErrorResponse(
    val statusCode: StatusCode,
    val errorInfo : String,
): DataResponse()