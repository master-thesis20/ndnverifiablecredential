package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.response.CommandResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import de.kthesis.lib.domain.named_verifiable_data_registry.service.IDataManagementService
import net.named_data.jndn.Name
import net.named_data.jndn.security.v2.Validator

/**
 * Insert Command of the NVDR Interest to insert data in the registry.
 */
class InsertCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val dataManagementService: IDataManagementService,
    validator: Validator,
) : AbstractCommand(registryCommandInterest, validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        // check if the interest is authorized to perform a data insert
        if (!authenticate()) return RegistryCommandResponseData(
            registryCommandInterest.name,
            CommandResponse(statusCode = StatusCode.AUTHORIZATION_FAILED)
        )

        // get the command parameter
        val commandParameter = registryCommandInterest.decodeParameter<CommandParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for INSERT command."
                )
            )

        // see https://redmine.named-data.net/projects/repo-ng/wiki/Basic_Repo_Insertion_Protocol

        val responseName = Name(registryCommandInterest.name)

        if (commandParameter.startBlockId != null
            && commandParameter.endBlockId != null
            && commandParameter.startBlockId > commandParameter.endBlockId
        ) {
            logger.info("Command Parameter of interest '${registryCommandInterest.name}' is not valid")
            // step 5 command parameter is not valid
            return RegistryCommandResponseData(
                responseName,
                CommandResponse(statusCode = StatusCode.COMMAND_MALFORMED)
            )
        } else {
            // step 7 insert data
            logger.info("Command Parameter is at valid, start insertion attempt")
            val startInsertionProcess = dataManagementService.insertData(
                commandParameter.data,
                commandParameter.startBlockId,
                commandParameter.endBlockId
            )
            logger.info("Send response")

            return RegistryCommandResponseData(
                responseName,
                CommandResponse(
                    startInsertionProcess.processId,
                    StatusCode.INSERTION_COMMAND_VALID,
                    startBlockId = commandParameter.startBlockId ?: 0,
                    endBlockId = commandParameter.endBlockId
                )
            )
        }
    }

}