package de.kthesis.lib.domain.named_verifiable_data_registry.response

import de.kthesis.lib.infrastructure.utils.serializer.NameSerializer
import kotlinx.serialization.Serializable
import net.named_data.jndn.Name

@Serializable
data class RevokeResponse(
    @Serializable(with = NameSerializer::class)
    val deregisteredCertificate : Name
) : DataResponse()
