package de.kthesis.lib.domain.named_verifiable_data_registry.response

import kotlinx.serialization.Serializable

@Serializable
data class NewResponse(
    val requestId : String,
    // for simplicity only string for challenge
    val challenge : String
) : DataResponse()
