package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.application.DataProvider
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ValidateParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.CertificateRepositoryEntry
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ChallengeResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.CommandResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import de.kthesis.lib.infrastructure.utils.cryptography.CertificateUtil
import net.named_data.jndn.Face
import net.named_data.jndn.Name
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.certificate.PublicKey
import net.named_data.jndn.security.v2.Validator

/**
 * VALIDATE Command of the NVDR Interest which signed the certificate request (obtained from the NEW Command) and stores the registered certificate.
 */
class ValidateCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
    private val certificateRepository: ICertificateRepository,
    private val repositoryPrefix : Name,
    private val repositoryKeyChain: KeyChain,
    private val face: Face,
    private val dataProvider: DataProvider,
    validator: Validator,
) : AbstractCommand(registryCommandInterest, validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        if (!authenticate()) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.AUTHORIZATION_FAILED,
                "VALIDATE Command Interest could not be authenticated."
            )
        )

        val validateParameter = registryCommandInterest.decodeParameter<ValidateParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for VALIDATE command."
                )
            )

        val entry = pendingRegistrationRepository.find(validateParameter.requestId)
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.NO_PENDING_REQUEST,
                    errorInfo = "No pending registration request for ${validateParameter.requestId} could be found."
                )
            )


        // challenge was accepted
        // not for real here, implement real challenge solving here
        // check if the key payload is actually a public key
        val publicKey = try {
            PublicKey(entry.certificate.publicKey)
        } catch (e: Exception) {
            // if an error occurred just return that the key format is wrong
            logger.warning("Could not parse key. See ${e.message}")
            return RegistryCommandResponseData(
                registryCommandInterest.name,
                CommandResponse(
                    statusCode = StatusCode.REGISTRATION_WRONG_KEY_FORMAT
                )
            )
        }

        // create registration certificate
        val certificate = CertificateUtil.createCertificateV2(
            identity = Name(repositoryPrefix).append(entry.nameSpace),
            // use the default key to sign registration certificates
            caPublicKeyName = repositoryKeyChain.pib.defaultIdentity.defaultKey.name,
            keyChain = repositoryKeyChain,
            publicKey = publicKey
        )

        // store the certificate
        if (!certificateRepository.add(CertificateRepositoryEntry(entry.nameSpace,certificate))) return RegistryCommandResponseData(
            registryCommandInterest.name,
            CommandResponse(
                statusCode = StatusCode.REGISTRATION_CERTIFICATE_ALREADY_EXISTS
            )
        )

        // store the certificate also in the pending registration repository
        val updateEntry = entry.copy(
            certificate = certificate
        )
        pendingRegistrationRepository.update(validateParameter.requestId,updateEntry)

        // register prefix to provide the inserted data
        face.registerPrefix(entry.nameSpace, dataProvider) {
            logger.warning("Could not register interest prefix for '${entry.nameSpace}'")
        }

        return createResponseData(
            ChallengeResponse(
                validateParameter.requestId,
                challengeType = "",
                statusCode = StatusCode.CHALLENGE_FULFILLED,
                certificateName = certificate.name
            )
        )
    }
}