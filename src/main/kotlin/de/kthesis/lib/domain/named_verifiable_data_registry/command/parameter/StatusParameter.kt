package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

/**
 * Parameter for the STATUS Interest for the registration process. Currently unused.\
 *
 * See https://named-data.net/wp-content/uploads/2017/04/ndn-0050-1-ndncert.pdf for more information.
 */
@kotlinx.serialization.Serializable
data class StatusParameter(
    val requestId : String,
) : InterestParameter()
