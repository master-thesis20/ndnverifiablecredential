package de.kthesis.lib.domain.named_verifiable_data_registry.service

import net.named_data.jndn.Name

/**
 * Basic service for the insertion and deletion of data according to the
 * NDN Repo protocol.
 */
interface IDataManagementService {

    /**
     * Start the insertion process for the given data name.
     * See https://redmine.named-data.net/projects/repo-ng/wiki/Basic_Repo_Insertion_Protocol for a more detailed description of the whole process.
     *
     * @param dataName The name of the data which should be inserted
     * @param startBlockId (Optional) The start block for the data.
     * Will be combined with the data name, e.g. "/prefix/data-name/0". Default 0.
     * @param endBlockId (Optional) The end block for the data.
     * If non given, the end block will be derived from the first inserted data packet.
     */
    suspend fun insertData(
        dataName: Name,
        startBlockId: Int? = null,
        endBlockId: Int? = null
    ): DataIOOperation

    /**
     * Start the deletion process for the given data name.
     * See https://redmine.named-data.net/projects/repo-ng/wiki/Repo_Deletion_Protocol for a more detailed description of the whole process.
     *
     * @param dataName The name of the data which should be deleted.
     * @param startBlockId (Optional) The start block for the data.
     * Will be combined with the data name, e.g. "/prefix/data-name/0". Default 0.
     * @param endBlockId (Optional) The end block for the data.
     * If non given, all data packets will be deleted
     */
   suspend fun deleteData(
       dataName: Name,
       startBlockId: Int? = null,
       endBlockId: Int? = null,
   ): DataIOOperation

    /**
     * Check if a data deletion is in progress.
     */
    suspend fun deleteDataInProgress(dataName: Name, startBlockId: Int?, endBlockId: Int?): Boolean
    suspend fun insertDataInProgress(dataName: Name, startBlockId: Int?, endBlockId: Int?): Boolean
}