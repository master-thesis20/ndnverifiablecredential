package de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure

import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import net.named_data.jndn.security.v2.CertificateFetcher
import net.named_data.jndn.security.v2.CertificateRequest
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.security.v2.ValidationError
import net.named_data.jndn.security.v2.ValidationError.CANNOT_RETRIEVE_CERTIFICATE
import net.named_data.jndn.security.v2.ValidationError.POLICY_ERROR
import net.named_data.jndn.security.v2.ValidationState
import java.util.logging.Logger

/**
 * Certificate Fetcher which search in the registry for the requested certificates.
 *
 * Will be used for the Validator in order to authenticate Interests by fetching the used certificates.
 */
class RegistryCertificateFetcher(
    private val rootCertificateList: List<CertificateV2>,
    private val certificateRepository: ICertificateRepository,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
) : CertificateFetcher() {

    override fun doFetch(
        certificateRequest: CertificateRequest?,
        validationState: ValidationState?,
        validationContinuation: ValidationContinuation?
    ) {
        // check if key name of the certificate exists
        val keyName = certificateRequest?.interest_?.name
            ?: return validationState?.fail(ValidationError(POLICY_ERROR)) ?: Unit

        Logger.getLogger(this::class.simpleName).info("Load certificate for $keyName")

        // find stored certificate, else raise error
        val certificate = certificateRepository.find(keyName)?.certificate
            ?: rootCertificateList.firstOrNull { certificateV2 -> certificateV2.keyName == keyName }
            ?: pendingRegistrationRepository.findAll().firstOrNull { it.nameSpace == keyName }?.certificate
            ?: return validationState?.fail(ValidationError(CANNOT_RETRIEVE_CERTIFICATE)) ?: Unit

        // continue the packet validation
        validationContinuation?.continueValidation(
            certificate, validationState
        )
    }
}