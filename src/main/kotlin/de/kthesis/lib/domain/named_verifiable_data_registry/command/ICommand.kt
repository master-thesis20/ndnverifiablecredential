package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData

/**
 * Obtained from a NVDR Interest which intend to control/update the registry
 */
internal interface ICommand {

    /**
     * Verifies if the given command is authenticated to be executed
     */
    suspend fun authenticate() : Boolean

    /**
     * Execute the given command and returns a response data
     */
    suspend fun execute() : RegistryCommandResponseData
}