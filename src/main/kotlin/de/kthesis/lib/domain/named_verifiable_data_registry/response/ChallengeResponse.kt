package de.kthesis.lib.domain.named_verifiable_data_registry.response

import de.kthesis.lib.infrastructure.utils.serializer.NameSerializer
import net.named_data.jndn.Name

/**
 * Response parameter for SELECT, VALIDATE and STATUS Interests.
 */
@kotlinx.serialization.Serializable
data class ChallengeResponse(
    val requestId : String,
    val challengeType : String,
    val statusCode: StatusCode,
    @kotlinx.serialization.Serializable(with = NameSerializer::class)
    val certificateName : Name?
) : DataResponse()

