package de.kthesis.lib.domain.named_verifiable_data_registry.service

import de.kthesis.lib.application.DataProvider
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.named_data.jndn.Data
import net.named_data.jndn.Face
import net.named_data.jndn.Name
import net.named_data.jndn.security.v2.CertificateV2
import java.util.logging.Logger
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * Manage the insertion and deletion for the data according to the NDN Repo protocol.
 *
 * See https://redmine.named-data.net/projects/repo-ng/wiki/Repo_Protocol_Specification for more information.
 *
 * @param face NDN Interface to express Interests for data
 * @param dataRepository Repository to store/delete data
 * @param dataProvider Used to provide the stored data for each receiving Interest
 */
class DataManagementService(
    private val face: Face,
    private val dataRepository: IDataRepository,
    private val dataProvider: DataProvider
) : IDataManagementService {

    private val logger = Logger.getLogger(this::class.java.simpleName)
    private val registeredPrefixes = mutableMapOf<Name,Long>()

    // separate manager for each operation type
    private val insertionOperationManager = DataIOOperationManager()
    private val deletionOperationManager = DataIOOperationManager()

    override suspend fun insertData(
        dataName: Name,
        startBlockId: Int?,
        endBlockId: Int?
    ): DataIOOperation = withContext(Dispatchers.IO) {
        val commandOperation = insertionOperationManager.createDataIOOperationIfNotExist(dataName)

        // do the data fetch asynchronous
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            var start = startBlockId
            // no endBlock missing timer implemented yet
            var end = endBlockId
            while (end == null || end > (start ?: 0)) {
                // construct the name for the packet segment
                var interestedDataName = Name(dataName)
                if (start != null) interestedDataName = interestedDataName.append(Name(start.toString()))

                // fetch data packet
                for (i in 0 until 3) {
                    // fetch data
                    logger.info("Try to fetch $interestedDataName")
                    val data = fetchData(interestedDataName) ?: continue
                    // store data or update data if already existing
                    if (!dataRepository.add(data)) dataRepository.update(data)


                    // register prefix for keyname if data is a certificate
                    if(CertificateV2.isValidName(data.name)) {
                        val keyName = CertificateV2.extractKeyNameFromCertName(data.name)
                        logger.info("Register Interest Prefix for $keyName")
                        // register prefix for key-name (certificates where fetches by key-name)
                        registeredPrefixes[keyName] = face.registerPrefix(Name(keyName), dataProvider) {
                            logger.warning("Could not register interest prefix for '${keyName}'")
                        }
                    }

                    // register prefix to provide the inserted data
                    registeredPrefixes[data.name] = face.registerPrefix(Name(data.name), dataProvider) {
                        logger.warning("Could not register interest prefix for '${data.name}'")
                    }

                    logger.info("Registered Interest Prefix for ${data.name}")

                    // update start and end block
                    if (data.metaInfo.finalBlockId != null
                        && data.metaInfo.finalBlockId.value.toString().isNotEmpty()
                    ) end = data.metaInfo.finalBlockId.value.toString().toInt()

                    if (start != null) start++
                    break
                }

                // update the insertion operation state information
                val currentState = insertionOperationManager.getDataIOOperation(dataName) ?: break
                if (currentState.startBlockId == start && currentState.endBlockId == end) break
                insertionOperationManager.updateDataIOOperation(dataName, start?:0, end)
            }
            // insertion is complete
            insertionOperationManager.removeDataIOOperation(dataName)
        }
        logger.info("Insert operation $commandOperation")
        return@withContext commandOperation
    }

    override suspend fun deleteData(
        dataName: Name,
        startBlockId: Int?,
        endBlockId: Int?
    ): DataIOOperation {
        val commandOperation = deletionOperationManager.createDataIOOperationIfNotExist(dataName)

        // remove prefix for keyname if data is certificate
        if(CertificateV2.isValidName(dataName)) {
            val keyName = CertificateV2.extractKeyNameFromCertName(dataName)
            registeredPrefixes[keyName]?.run(face::removeRegisteredPrefix)
        }
        registeredPrefixes[dataName]?.run(face::removeRegisteredPrefix)

        logger.info("Remove Interest Prefix for $dataName")

        // end block is missing, delete every segment
        if (endBlockId == null) {
            var dataSegmentPrefix = Name(dataName)
            if (startBlockId != null) dataSegmentPrefix =
                dataSegmentPrefix.append(Name(startBlockId.toString()))
            dataRepository.removeByPrefix(dataSegmentPrefix)
            return commandOperation
        }
        for (index in (startBlockId ?: 0) until endBlockId) {
            val dataSegmentName = Name(dataName).append(Name(startBlockId.toString()))
            // delete the data packet
            dataRepository.remove(dataSegmentName)
            deletionOperationManager.updateDataIOOperation(dataName, index, endBlockId)
        }
        deletionOperationManager.removeDataIOOperation(dataName)

        return commandOperation
    }

    override suspend fun deleteDataInProgress(
        dataName: Name,
        startBlockId: Int?,
        endBlockId: Int?
    ) = deletionOperationManager.hasDataIOOperation(dataName, startBlockId, endBlockId)

    override suspend fun insertDataInProgress(
        dataName: Name,
        startBlockId: Int?,
        endBlockId: Int?
    ) = insertionOperationManager.hasDataIOOperation(dataName, startBlockId, endBlockId)


    /**
     * Simple wrapper to express and Interest for the given data name and wait for the data packet (or a timeout)
     *
     * @param dataName The name of the data which should be fetched.
     */
    private suspend fun fetchData(dataName: Name): Data? = suspendCoroutine { continuation ->
        face.expressInterest(dataName, { _, data ->
            logger.info("Data $dataName could be fetched")
            continuation.resume(data)
        }) {
            // timeout occurs
            logger.info("Data $dataName could be not fetched")
            continuation.resume(null)
        }
    }
}