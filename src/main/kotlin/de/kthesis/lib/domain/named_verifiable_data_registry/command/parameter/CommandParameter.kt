package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

import de.kthesis.lib.infrastructure.utils.serializer.BlobSerializer
import de.kthesis.lib.infrastructure.utils.serializer.ForwardingHintSerializer
import de.kthesis.lib.infrastructure.utils.serializer.NameSerializer
import kotlinx.serialization.Serializable
import net.named_data.jndn.Name
import net.named_data.jndn.util.Blob

/**
 * Similar to the RepoCommandParameter object as described in the thesis.
 *
 * See https://redmine.named-data.net/projects/repo-ng/wiki/Repo_Command for more
 */
@Serializable
data class CommandParameter(
    @Serializable(with = NameSerializer::class)
    val data: Name,
    val startBlockId: Int? = null,
    val endBlockId: Int? = null,
    val processId: Int? = null,
    val interestLifetime: Int? = null,
    @Serializable(with = ForwardingHintSerializer::class)
    val forwardingHint: ForwardingHint? = null,
    @Serializable(with = BlobSerializer::class)
    val additionalPayLoad : Blob? = null,
) : InterestParameter()