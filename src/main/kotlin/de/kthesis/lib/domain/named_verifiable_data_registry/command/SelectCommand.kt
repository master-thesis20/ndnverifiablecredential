package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.SelectParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ChallengeResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import net.named_data.jndn.security.v2.Validator

/**
 * SELECT Command of the NVDR Interest to pick a challenge (just runs in the current implementation)
 *
 * Part of the registration process.
 */
class SelectCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
    validator: Validator
) :
    AbstractCommand(registryCommandInterest, validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        if (!authenticate()) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.AUTHORIZATION_FAILED,
                "SELECT Command Interest could not be authenticated."
            )
        )

        val selectParameter = registryCommandInterest.decodeParameter<SelectParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for SELECT command."
                )
            )

        val entry = pendingRegistrationRepository.find(selectParameter.requestId)
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.NO_PENDING_REQUEST,
                    errorInfo = "No pending registration request for ${selectParameter.requestId} could be found."
                )
            )

        // pick challenge and response with parameter

        // here just runs and returns successful message (no challenge implemented)
        return createResponseData(
            ChallengeResponse(
                selectParameter.requestId,
                challengeType = selectParameter.challenge,
                statusCode = StatusCode.CHALLENGE_SELECTED,
                certificateName = null
            )
        )
    }
}