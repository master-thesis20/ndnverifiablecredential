package de.kthesis.lib.domain.named_verifiable_data_registry.response

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.named_data.jndn.Data
import net.named_data.jndn.Name
import net.named_data.jndn.util.Blob
import java.util.logging.Logger

class RegistryCommandResponseData(
    dataName: Name,
    val parameterResponse: String
) : Data(dataName) {

    constructor(data: Data) : this(
        data.name,
        data.content.toString()
    )

    constructor(dataName: Name, parameterResponse: DataResponse)
            : this(dataName, Json.encodeToString(parameterResponse))

    init {
        content = Blob(parameterResponse)
    }

    /**
     * Try to decode the Data's content to a pre-defined object
     */
    inline fun <reified T : DataResponse> decodeParameter(): T? = try {
        Json.decodeFromString<DataResponse>(content.toString()) as T
    } catch (e: Exception) {
        Logger.getLogger(this::class.java.simpleName)
            .warning("Could not decode parameter response. See ${e.message}")
        null
    }
}