package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

import net.named_data.jndn.Name

/**
 * Not actively used yet, is used for the Repo Command Parameter
 */
data class ForwardingHint(
    val forwardNames : List<Name>
)