package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

/**
 * Parameter for the SELECT Interest in the registration process. Challenge is currently not implemented and just a string.
 *
 * See https://named-data.net/wp-content/uploads/2017/04/ndn-0050-1-ndncert.pdf for more information.
 */
@kotlinx.serialization.Serializable
class SelectParameter(
    val requestId : String,
    // simple string since challenge implementation it out of scope
    val challenge : String,
) : InterestParameter()