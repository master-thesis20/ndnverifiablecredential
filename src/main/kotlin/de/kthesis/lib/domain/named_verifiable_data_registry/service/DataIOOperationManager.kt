package de.kthesis.lib.domain.named_verifiable_data_registry.service

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import net.named_data.jndn.Name

/**
 * Holds and manage information about insert/deletion process.
 */
class DataIOOperationManager {

    // simple hash map later replace with database
    private val operationMap = HashMap<Name, DataIOOperation>()
    private val mutex = Mutex()

    /**
     * Creates a new entry for an insert/deletion process for the given data name
     *
     * @param dataName The name of the data which is edited (inserted/deleted)
     * @return A entry for the data operation.
     */
    suspend fun createDataIOOperationIfNotExist(dataName: Name): DataIOOperation = mutex.withLock {
        // find the process id
        if (operationMap.contains(dataName)) return operationMap[dataName]!!

        // create a new unused process id
        for (processId in 0 until Int.MAX_VALUE) {
            if (operationMap.values.any { commandOperation -> commandOperation.processId == processId }) continue
            operationMap[dataName] = DataIOOperation(processId, 0, null)
            // found a new one
            break
        }

        // return the new entry
        return operationMap[dataName]!!
    }

    /**
     * Checks if the manager has a data operation entry for the given data name
     *
     * @param dataName The name of the data which is edited (inserted/deleted)
     * @param startBlockId The start block id of the data name
     * @param endBlockId The end block id of the data name
     * @return true if a entry for the given name, start and end block id exist, false otherwise
     */
    suspend fun hasDataIOOperation(dataName: Name,startBlockId: Int?, endBlockId: Int?) = mutex.withLock {
        return@withLock operationMap[dataName]?.run {
            this.startBlockId == startBlockId && this.endBlockId == endBlockId
        }?:false
    }

    /**
     * Search for an operation information entry.
     *
     * @param dataName The name of a data packet for which an io operation is searched
     * @return The operation entry for the given name, null if none exist
     */
    suspend fun getDataIOOperation(dataName: Name) = mutex.withLock {
        operationMap[dataName]
    }

    /**
     * Updates an existing operation information with the given parameter.
     * Nothing happens if no operation entry exist.
     * Use [getDataIOOperation] to obtain the current operation information.
     *
     * @param dataName The name of the data which is edited (inserted/deleted)
     * @param startBlockId The (new) start block id of the data name
     * @param endBlockId The (new) end block id of the data name
     */
    suspend fun updateDataIOOperation(
        dataName: Name,
        startBlockId: Int,
        endBlockId: Int?
    ) = mutex.withLock {
        val state = operationMap[dataName]?:return@withLock
        operationMap[dataName] = state.copy(startBlockId = startBlockId, endBlockId = endBlockId)
    }

    /**
     * Deletes the stored information for the given data name
     *
     * @param dataName The name of the data which is edited (inserted/deleted)
     */
    suspend fun removeDataIOOperation(dataName: Name) {
        mutex.withLock {
            operationMap.remove(dataName)
        }
    }
}