package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.response.DataResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.infrastructure.utils.validate
import net.named_data.jndn.security.v2.Validator
import java.util.logging.Logger

/**
 * Base class for NVDR Commands obtained from NVDR Interests
 *
 */
abstract class AbstractCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val validator: Validator
) : ICommand {

    protected val logger = Logger.getLogger(this::class.simpleName)

    protected fun createResponseData(responseParameter: DataResponse) =
        RegistryCommandResponseData(registryCommandInterest.name, responseParameter)

    /**
     * authenticate method is currently for most of the methods the same as they used the validator config as trust schema
     *
     * Like describes in the thesis.
     *
     * @return true if the Interest of the command is valid
     */
    override suspend fun authenticate(): Boolean {
        // simple verify if the command is signed according to the policy
        // implement here further access policy if desired
        val error = validator.validate(registryCommandInterest)?: return true
        logger.warning("Interest ${registryCommandInterest.name} could not be verified. See $error")
        return false
    }
}