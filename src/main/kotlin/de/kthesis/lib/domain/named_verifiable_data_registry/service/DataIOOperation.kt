package de.kthesis.lib.domain.named_verifiable_data_registry.service

/**
 * information about the insert/deletion operations
 */
data class DataIOOperation(val processId: Int, val startBlockId: Int, val endBlockId: Int?)