package de.kthesis.lib.domain.named_verifiable_data_registry.repository

/**
 * Base CRUD Repository Interface
 */
interface IRepository<K,V> {

    fun add(value : V) : Boolean

    fun remove(key : K) : V?

    fun find(key: K) : V?

    fun update(value : V) : Boolean
}