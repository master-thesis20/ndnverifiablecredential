package de.kthesis.lib.domain.named_verifiable_data_registry.response

import de.kthesis.lib.infrastructure.utils.serializer.NameSerializer
import kotlinx.serialization.Serializable
import net.named_data.jndn.Name

/**
 * Similar to the RepoCommandResponse object as describes in the thesis.
 *
 * See https://redmine.named-data.net/projects/repo-ng/wiki/Repo_Command for more
 */
@Serializable
data class CommandResponse(
    // refers to the insertion or deletion process
    val processId : Int = -1,
    val statusCode : StatusCode,
    val startBlockId : Int? = null,
    val endBlockId : Int? = null,
    val insertNum  : Int = 0,
    val deleteNum : Int = 0,
    @Serializable(with = NameSerializer::class)
    val dataName : Name? = null,
) : DataResponse()