package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import net.named_data.jndn.security.v2.Validator

/**
 * STATUS Command of the NVDR Interest to check the registration process.
 *
 * NOT IMPLEMENTED YET (not used)
 */
class StatusCommand(val registryCommandInterest: RegistryCommandInterest, validator: Validator) :
    AbstractCommand(registryCommandInterest, validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        // TODO not implemented yet because not needed
        // implement if the registration challenge is real
        return createResponseData(ErrorResponse(StatusCode.INTERNAL_ERROR,"SELECT is not implemented yet"))
    }
}