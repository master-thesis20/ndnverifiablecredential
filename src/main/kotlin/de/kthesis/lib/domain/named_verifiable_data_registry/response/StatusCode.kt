package de.kthesis.lib.domain.named_verifiable_data_registry.response

/**
 * Contains status code for the various NVDR Interest responses
 */
enum class StatusCode(val statusCode: Int) {

    COMMAND_MALFORMED(403),
    INTERNAL_ERROR(500),


    INSERTION_COMMAND_VALID(100),
    INSERTION_DONE(200),
    INSERTION_IN_PROGRESS(300),

    INSERTION_NO_SUCH_PROCESS(404),

    DELETION_DONE(200),
    DELETION_IN_PROGRESS(300),
    DELETION_NO_SUCH_DELETION(404),


    /**
     * created certificate with the given name already exist
     */
    REGISTRATION_CERTIFICATE_ALREADY_EXISTS(408),

    /**
     * the format of the public key is false
     */
    REGISTRATION_WRONG_KEY_FORMAT(404),
    /**
     * Certificate was inserted
     */
    REGISTRATION_COMMAND_DONE(200),

    /**
     * The desired name space is already used
     */
    PROBE_NAMESPACE_USED(409),

    /**
     * The parameter could not be parsed
     */
    PARAMETER_INVALID(422),

    AUTHORIZATION_FAILED(401),

    NO_PENDING_REQUEST(404),

    /**
     * The selected challenge was marked as selected
     */
    CHALLENGE_SELECTED(200),

    /**
     * Challenge was fulfiled
     */
    CHALLENGE_FULFILLED(200),

    /**
     * NO certificate can be doanloaded
     */
    NO_CERTIFICATE(404),
}
