package de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure

/**
 * Runtime CRUD repository.
 *
 * Just for demo purposes.
*/
abstract class Repository<K, V>{
    
    protected val runtimeStorage : MutableMap<K,V> = mutableMapOf()

    protected fun _add(key : K, value: V): Boolean {
        if (runtimeStorage.contains(key)) return false
        runtimeStorage[key] = value
        return true
    }

    fun remove(key: K): V? = runtimeStorage.remove(key)

    fun find(key: K): V? = runtimeStorage[key]

    protected fun _update(key: K, value: V): Boolean {
        if (!runtimeStorage.contains(key)) return false
        runtimeStorage[key] = value
        return true
    }


}