package de.kthesis.lib.domain.named_verifiable_data_registry

import de.kthesis.lib.domain.named_verifiable_data_registry.command.CommandType
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.CertificateRepositoryEntry
import de.kthesis.lib.infrastructure.utils.components
import net.named_data.jndn.Name
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.security.v2.ValidationPolicyConfig
import net.named_data.jndn.util.Common

/**
 * Access policy to authorize the received command interests.
 *
 * Can be passed to a ndn Validator class to apply the policy
 */
class CommandVerificationConfig(
    private val repoPrefix: Name,
    rootCertificateList: List<CertificateV2>
) :
    ValidationPolicyConfig() {

    /**
     * contains the entries for the trust anchors
     */
    private val trustAnchorEntries = rootCertificateList.map(::createTrustAnchorEntry)

    /**
     * returns a regex string for the given name in the format <component0><component1>... etc
     */
    private fun Name.regex() = this.components().joinToString(separator = "") { component -> "<$component>" }

    /**
     * Create a rule for the authentication of the registration interests.
     *
     * @param reservedNameSpace The name space which was reserved with the _PROBE interest
     * @param certificateRequest The certificate request which is basically a self-signed certificate, obtained from the _NEW Interest.
     *
     * @return A rule which authenticates the follow up registrations for the registration only if the certificate request was used.
     */
    private fun createRegistrationTrustRuleAndTrustAnchor(reservedNameSpace : Name, certificateRequest : CertificateV2): String =
        """
            rule
            {
                id "Valid Registration NVDR Interest for "$reservedNameSpace"
                for interest
                filter 
                {
                    type name
                    regex ^${repoPrefix.regex()}${reservedNameSpace.regex()}[<${CommandType.VALIDATE}><${CommandType.SELECT}><${CommandType.STATUS}>]
                }
                checker
                {
                    type customized
                    sig-type rsa-sha256
                    key-locator
                    {
                        type name
                        name ${/*implementation bug forces using identity instead of keylocator */ certificateRequest.identity}
                        relation equal
                    }	
                }
            }
            trust-anchor
            {
                type base64
                base64-string "${Common.base64Encode(certificateRequest.wireEncode().immutableArray)}"
            }
        """.trimIndent()

    /**
     * Creates trust rule for data management interests after certificate registration
     *
     * @param assignedNameSpace The name which was assigned during the registration process.
     * @param registeredCertificateV2 The signed certificate request from the registration process.
     * @return A trust rule for the authentication of the data management Interests for the assigned name space.
     */
    private fun createDataManagementTrustRule(assignedNameSpace : Name, registeredCertificateV2: CertificateV2) : String =
        """
            rule 
            {
                id "Valid Data Management NVDR Interest for "$assignedNameSpace"
                for interest
                filter 
                {
                    type name 
                    regex ^${repoPrefix.regex()}${assignedNameSpace.regex()}[<${CommandType.INSERT}><${CommandType.INSERT_CHECK}><${CommandType.DELETE}><${CommandType.DELETE_CHECK}>]
                }
                checker 
                {
                    type customized
                    sig-type rsa-sha256
                    key-locator
                    {
                        type name
                        name ${/*implementation bug forces using identity instead of keylocator */ registeredCertificateV2.identity}
                        relation equal
                    }	
                }
            }
        """.trimIndent()

    private fun createDeRegisterTrustRule(registeredCertificateV2: CertificateV2)  =
        """
            rule 
            {
                id "Valid De-Register NVDR Interest
                for interest
                filter 
                {
                    type name
                    regex ^${repoPrefix.regex()}<${CommandType.DEREGISTER}>${registeredCertificateV2.name.regex()}
                }
                checker
                {
                    type customized
                    sig-type rsa-sha256
                    key-locator
                    {
                        type name 
                        name ${/*implementation bug forces using identity instead of keylocator */ registeredCertificateV2.identity}
                        relation equal
                    }	
                }
            }
        """.trimIndent()



    private fun createTrustAnchorEntry(rootCertificateV2: CertificateV2) =
        """
            trust-anchor
            {
                type base64
                base64-string "${Common.base64Encode(rootCertificateV2.wireEncode().immutableArray)}"
            }
        """.trimIndent()


    private fun getFullValidatorConfigUpdated(registeredCertificates: List<CertificateRepositoryEntry>, pendingCertificateV2: List<CertificateRepositoryEntry>): String = """
        validator
            {
                ${pendingCertificateV2.filter{ it.certificate.name != Name()}.joinToString(separator = "\n") { createRegistrationTrustRuleAndTrustAnchor(it.nameSpace,it.certificate)}}
                ${registeredCertificates.joinToString(separator = "\n") { createDataManagementTrustRule(it.nameSpace, it.certificate)}}
                ${registeredCertificates.joinToString(separator = "\n") { createDeRegisterTrustRule(it.certificate) }}
                rule {
                    id "Certificate Validation Rule"
                    for data
                    checker
                    {
                        type hierarchical
                        sig-type rsa-sha256
                    }
                }
                ${trustAnchorEntries.joinToString(separator = "\n")}
            }
        }"""



    // TODO updates trust rules to contains the desired name space, too
    fun loadConfig(registeredCertificates: List<CertificateRepositoryEntry>, pendingCertificateV2: List<CertificateRepositoryEntry>) {
        val config = getFullValidatorConfigUpdated(registeredCertificates,pendingCertificateV2)
        //Logger.getLogger(this::class.java.simpleName).info("ValidatorConfig $config")
        load(config, "config")
        //
    }


}