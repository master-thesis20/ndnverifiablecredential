package de.kthesis.lib.domain.named_verifiable_data_registry.response

import de.kthesis.lib.infrastructure.utils.serializer.CertificateSerializer
import net.named_data.jndn.security.v2.CertificateV2

@kotlinx.serialization.Serializable
data class DownloadResponse(
    @kotlinx.serialization.Serializable(with = CertificateSerializer::class)
    val certificateV2: CertificateV2?
) : DataResponse()

