package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

import de.kthesis.lib.infrastructure.utils.serializer.CertificateSerializer
import kotlinx.serialization.Serializable
import net.named_data.jndn.security.v2.CertificateV2

/**
 * Contains information for the NEW Interest, like the id of the registration process and the certificate request.
 *
 * See https://named-data.net/wp-content/uploads/2017/04/ndn-0050-1-ndncert.pdf for more information.
 */
@Serializable
data class NewParameter(
    // differs from ndncert
    val requestId : String,
    @Serializable(with = CertificateSerializer::class)
    val certificateRequest: CertificateV2
) : InterestParameter()