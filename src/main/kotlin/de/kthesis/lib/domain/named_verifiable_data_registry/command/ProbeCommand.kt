package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ProbeParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.CertificateRepositoryEntry
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ProbeResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.security.v2.Validator

/**
 * PROBE Command of the NVDR Interest to reserve a name space.
 *
 * First Interest of the registration process.
 */
class ProbeCommand(
    val registryCommandInterest: RegistryCommandInterest,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
    private val certificateRepository: ICertificateRepository,
    validator: Validator
) :
    AbstractCommand(registryCommandInterest, validator) {

    override suspend fun authenticate(): Boolean {
        // PROBE interest is always authenticated
        // (just for now it can be later restricted who can start register process)
        return true
    }

    override suspend fun execute(): RegistryCommandResponseData {
        if (!authenticate()) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.AUTHORIZATION_FAILED,
                "PROBE Command Interest could not be authenticated."
            )
        )

        val probeParameter = registryCommandInterest.decodeParameter<ProbeParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for PROBE command."
                )
            )

        // check if the desired name space is still free
        if (pendingRegistrationRepository.hasNameSpace(probeParameter.desiredNameSpace)
            || certificateRepository.hasNameSpace(probeParameter.desiredNameSpace)
        ) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.PROBE_NAMESPACE_USED,
                errorInfo = "Name space ${probeParameter.desiredNameSpace} is already in use."
            )
        )

        // store registration request
        val requestId = pendingRegistrationRepository.add(
            CertificateRepositoryEntry(
                probeParameter.desiredNameSpace,
                // just add an empty certificate
                certificate = CertificateV2()
            )
        )

        // create response with request id
        return createResponseData(
            ProbeResponse(
                // simply use name space as request id
                requestId = requestId,
                identifier = probeParameter.desiredNameSpace.toString(),
                caInfo = ""
            )
        )
    }
}