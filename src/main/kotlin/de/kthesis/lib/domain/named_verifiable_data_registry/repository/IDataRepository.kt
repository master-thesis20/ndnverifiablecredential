package de.kthesis.lib.domain.named_verifiable_data_registry.repository

import net.named_data.jndn.Data
import net.named_data.jndn.Name

/**
 * Repository which stores inserted data and provide the possibility to retrieve the data
 */
interface IDataRepository : IRepository<Name, Data> {

    fun removeByPrefix(prefix : Name)

    fun findFirstByPrefix(prefix: Name) : Data?

    fun findAll(): MutableCollection<Data>
}