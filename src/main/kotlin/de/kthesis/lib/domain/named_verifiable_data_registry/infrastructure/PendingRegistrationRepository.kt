package de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure

import de.kthesis.lib.domain.named_verifiable_data_registry.repository.CertificateRepositoryEntry
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import net.named_data.jndn.Name
import java.util.Random
import kotlin.streams.asSequence

/**
 * Runtime repository to store information for the registration process. Does not store any information persistent.
 *
 * Just for demo purposes.
 */
class PendingRegistrationRepository : IPendingRegistrationRepository,
    Repository<String, CertificateRepositoryEntry>() {

    override fun hasNameSpace(nameSpace: Name): Boolean =
        runtimeStorage.values.any { pendingRegistrationEntry ->
            pendingRegistrationEntry.nameSpace.isPrefixOf(nameSpace)
                    || nameSpace.isPrefixOf(pendingRegistrationEntry.nameSpace)
        }

    override fun add(value: CertificateRepositoryEntry): String {
        val random = Random(2)
        var processId: String
        do {
            processId = random.ints(10, 0, 9).asSequence().joinToString(separator = "")
        } while (runtimeStorage.keys.contains(processId))

        _add(processId, value)
        return processId
    }

    override fun update(requestId: String, entry: CertificateRepositoryEntry): Boolean =
        _update(requestId, entry)

    override fun remove(certificateName: Name) {
        val key = runtimeStorage.entries.filter { (_,entry) ->
            entry.certificate.name == certificateName
        }.firstOrNull()?.key?:return

        runtimeStorage.remove(key = key)
    }

    override fun findAll(): List<CertificateRepositoryEntry> = runtimeStorage.values.toList()
}