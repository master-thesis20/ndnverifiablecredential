package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.NewParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.NewResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import net.named_data.jndn.security.v2.Validator

/**
 * NEW Command of the NVDR Interest to submit a certificate request which should be signed.
 *
 * Needed for the registration processes.
 */
class NewCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
    validator: Validator) :
    AbstractCommand(registryCommandInterest, validator) {

    override suspend fun authenticate(): Boolean {
        // NEW interest is always authenticated
        // (just for now it can be  later restricted how can start register process)
        return true
    }

    override suspend fun execute(): RegistryCommandResponseData {
        if (!authenticate()) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.AUTHORIZATION_FAILED,
                errorInfo = "NEW Command Interest could not be authenticated"
            )
        )

        val newParameter = registryCommandInterest.decodeParameter<NewParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for NEW command."
                )
            )

        /*
        return error message of the request id is not found
        maybe default behaviour could be the creation of a registration entry with the
        certificate and the name associated to the certificate
         */
        val pendingRegistrationEntry = pendingRegistrationRepository.find(newParameter.requestId)?: return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.NO_PENDING_REQUEST,
                errorInfo = "No pending registration request for ${newParameter.requestId} could be found."
            )
        )

        val updatedEntry = pendingRegistrationEntry.copy(
            certificate = newParameter.certificateRequest
        )

        // update the entry with the received certificate
        pendingRegistrationRepository.update(newParameter.requestId,updatedEntry)

        return createResponseData(
            NewResponse(
                requestId = newParameter.requestId,
                // challenge is not implemented
                challenge = "trust-me"
            )
        )
    }
}