package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.repository.ICertificateRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IPendingRegistrationRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RevokeResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import net.named_data.jndn.Name
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.security.v2.Validator


/**
 * De-Register Command for the NVDR Interest to delete registered certificates.
 *
 * It is not possible to insert/delete data after the registered certificate is deleted.
 *
 * A new certificate need to be registered, before doing so.
 */
class DeRegisterCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val certificateRepository: ICertificateRepository,
    private val pendingRegistrationRepository: IPendingRegistrationRepository,
    validator: Validator
) : AbstractCommand(registryCommandInterest, validator) {


    override suspend fun execute(): RegistryCommandResponseData {
        if (!authenticate()) return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.AUTHORIZATION_FAILED,
                "REVOKE Command Interest could not be authenticated."
            )
        )

        val suffix = registryCommandInterest.suffix?.getPrefix(-2)?: return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.PARAMETER_INVALID,
                "Certificate name could not be obtained from interest."
            )
        )

        val certificateName = Name(registryCommandInterest.parameter?.string).append(suffix)

        // remove pending interest
        pendingRegistrationRepository.remove(certificateName)
        // remove certificate
        certificateRepository.remove(CertificateV2.extractKeyNameFromCertName(certificateName))

        return createResponseData(
            RevokeResponse(certificateName)
        )
    }

}