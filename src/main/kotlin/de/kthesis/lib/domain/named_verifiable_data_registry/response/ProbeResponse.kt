package de.kthesis.lib.domain.named_verifiable_data_registry.response

@kotlinx.serialization.Serializable
data class ProbeResponse(
    // add request id
    val requestId : String,
    val identifier : String,
    val caInfo : String
) : DataResponse()

