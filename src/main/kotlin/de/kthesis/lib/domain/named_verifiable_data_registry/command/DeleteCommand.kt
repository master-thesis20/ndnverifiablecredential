package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.response.CommandResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import de.kthesis.lib.domain.named_verifiable_data_registry.service.IDataManagementService
import de.kthesis.lib.infrastructure.utils.plus
import net.named_data.jndn.security.v2.Validator

/**
 * Delete Command of the NVDR Interest to delete stored data from the registry
 */
class DeleteCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val dataManagementService: IDataManagementService,
    validator: Validator
) : AbstractCommand(registryCommandInterest,validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        //  check if the interest is authorized to perform a data deletion
        if (!authenticate()) return RegistryCommandResponseData(
            registryCommandInterest.name,
            CommandResponse(statusCode = StatusCode.AUTHORIZATION_FAILED)
        )

        // get the command parameter
        val commandParameter = registryCommandInterest.decodeParameter<CommandParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for DELETE command."
                )
            )

        // response name is <interest name>/<parameter data name>
        val responseName = registryCommandInterest.name + commandParameter.data

        // see https://redmine.named-data.net/projects/repo-ng/wiki/Repo_Deletion_Protocol
        // step 3 - selectors are not part of current specification

        return commandParameter.run {
            when {
                (startBlockId != null
                        && endBlockId != null
                        && startBlockId > endBlockId) -> {

                    // step 6
                    RegistryCommandResponseData(
                        responseName,
                        CommandResponse(statusCode = StatusCode.COMMAND_MALFORMED)
                    )
                }
                dataManagementService.deleteDataInProgress(data, startBlockId, endBlockId) -> {
                    val deletionProcess =
                        dataManagementService.deleteData(data, startBlockId, endBlockId)
                    RegistryCommandResponseData(
                        responseName,
                        CommandResponse(
                            deletionProcess.processId,
                            StatusCode.DELETION_DONE,
                            deletionProcess.startBlockId,
                            deletionProcess.endBlockId
                        )
                    )
                }
                else -> {
                    val startDeletionProcess =
                        dataManagementService.deleteData(data, startBlockId, endBlockId)

                    RegistryCommandResponseData(
                        responseName,
                        CommandResponse(
                            startDeletionProcess.processId,
                            StatusCode.DELETION_DONE,
                            startDeletionProcess.startBlockId,
                            startDeletionProcess.endBlockId
                        )
                    )
                }
            }
        }
    }

}