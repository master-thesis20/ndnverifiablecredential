package de.kthesis.lib.domain.named_verifiable_data_registry

import de.kthesis.lib.application.DataProvider
import de.kthesis.lib.application.InterestHandler
import de.kthesis.lib.application.RepositoryConfig
import de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure.RegistryCertificateFetcher
import de.kthesis.lib.domain.named_verifiable_data_registry.service.DataManagementService
import de.kthesis.lib.domain.named_verifiable_data_registry.service.IDataManagementService
import de.kthesis.lib.infrastructure.di.repositoryModule
import net.named_data.jndn.Face
import net.named_data.jndn.Interest
import net.named_data.jndn.InterestFilter
import net.named_data.jndn.Name
import net.named_data.jndn.OnInterestCallback
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.v2.CertificateV2
import net.named_data.jndn.security.v2.Validator
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.mp.KoinPlatformTools
import java.util.logging.Logger

/**
 * Main class to set up a named verifiable data registry.
 *
 * Can be simply passed to a [Face.registerPrefix] Method and handles all Interests for the registry.
 *
 * @param nvdrPrefix The prefix of the NDN Node ,e.g., "/nur"
 * @param face The interface for the NDN Communication
 * @param keyChain Should contain the private keys associated to the [rootCertificateList] keys
 * @param rootCertificateList Contains all certificates which serve as signing certificates
 * @param logger A custom logger
 */
class NamedVerifiableDataRegistry(
    internal val nvdrPrefix: Name,
    internal val face: Face,
    internal val keyChain: KeyChain,
    internal val rootCertificateList: List<CertificateV2>,
    internal val logger: Logger
) : OnInterestCallback, KoinComponent {

    // lazy injection enables to init dependency in koin
    private val interestHandler by inject<InterestHandler>()

    init {
        // Inject all dependencies
        val moduleList = listOf(
            repositoryModule,
            module {
                // TODO move this a separate module file
                single { RepositoryConfig(nvdrPrefix, keyChain) }
                single { DataProvider(get(),get()) }
                single { CommandVerificationConfig(nvdrPrefix, rootCertificateList) }
                single { RegistryCertificateFetcher(rootCertificateList, get(), get()) }
                single<IDataManagementService> { DataManagementService(face, get(), get()) }
                single { Validator(get<CommandVerificationConfig>(), get<RegistryCertificateFetcher>())}
                single { InterestHandler(get(), get(), get(), get(), get(), get(), this@NamedVerifiableDataRegistry.logger,face,get(), get()) }
            }
        )

        // TODO fails when 2 instances are created due to duplicate koin instances
        KoinPlatformTools.defaultContext().getOrNull()?.loadModules(moduleList) ?: startKoin {
            modules(moduleList)
        }
    }


    override fun onInterest(
        prefix: Name?,
        receivedInterest: Interest?,
        face: Face?,
        interestFilterId: Long,
        interestFilter: InterestFilter?
    ) {
        if (prefix != nvdrPrefix) return
        if (receivedInterest == null) return

        interestHandler.handleInterest(receivedInterest, face)
    }

}