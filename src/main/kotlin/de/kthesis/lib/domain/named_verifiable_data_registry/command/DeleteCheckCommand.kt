package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import de.kthesis.lib.domain.named_verifiable_data_registry.response.CommandResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ErrorResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import de.kthesis.lib.domain.named_verifiable_data_registry.service.IDataManagementService
import net.named_data.jndn.security.v2.Validator

/**
 * Delete Check Command of the NVDR Interest to delete inserted data.
 *
 * Check if the deletion of the data is in progress or already done.
 */
class DeleteCheckCommand(
    private val registryCommandInterest: RegistryCommandInterest,
    private val dataManagementService: IDataManagementService,
    private val dataRepository: IDataRepository,
    validator: Validator
) : AbstractCommand(registryCommandInterest, validator) {

    override suspend fun execute(): RegistryCommandResponseData {
        // check if the interest is authorized to perform a data delete check
        if (!authenticate()) return RegistryCommandResponseData(
            registryCommandInterest.name,
            CommandResponse(statusCode = StatusCode.AUTHORIZATION_FAILED)
        )

        // get the command parameter
        val commandParameter = registryCommandInterest.decodeParameter<CommandParameter>()
            ?: return createResponseData(
                ErrorResponse(
                    statusCode = StatusCode.PARAMETER_INVALID,
                    errorInfo = "Could not parse parameter for DELETE CHECK command."
                )
            )

        // check if the data deletion is currently executed
        if (dataManagementService.deleteDataInProgress(
                commandParameter.data,
                commandParameter.startBlockId,
                commandParameter.endBlockId
            )
        ) {
            return createResponseData(
                CommandResponse(statusCode = StatusCode.DELETION_IN_PROGRESS)
            )
        }

        // if not executed, check if the data was already deleted
        dataRepository.find(commandParameter.data) ?: return createResponseData(
            CommandResponse(statusCode = StatusCode.DELETION_DONE)
        )

        // return data is still there
        return createResponseData(
            ErrorResponse(
                statusCode = StatusCode.DELETION_NO_SUCH_DELETION,
                errorInfo = "Data is not deleted and not intend to do so could be found."
            )
        )
    }

}