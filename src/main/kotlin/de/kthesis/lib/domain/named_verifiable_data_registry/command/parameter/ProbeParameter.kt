package de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter

import de.kthesis.lib.infrastructure.utils.serializer.NameSerializer
import kotlinx.serialization.Serializable
import net.named_data.jndn.Name

/**
 * Parameter for the PROBE Interest for the registration process. Contains the desired name space.
 *
 * See https://named-data.net/wp-content/uploads/2017/04/ndn-0050-1-ndncert.pdf for more information.
 */
@Serializable
data class ProbeParameter(
    @Serializable(with = NameSerializer::class)
    val desiredNameSpace : Name
): InterestParameter()