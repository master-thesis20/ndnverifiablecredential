package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.DownloadParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.InterestParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.NewParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ProbeParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.SelectParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ValidateParameter
import de.kthesis.lib.infrastructure.utils.components
import de.kthesis.lib.infrastructure.utils.minus
import de.kthesis.lib.infrastructure.utils.plus
import de.kthesis.lib.infrastructure.values.URLDecodedString
import de.kthesis.lib.infrastructure.values.URLEncodedString
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.named_data.jndn.Interest
import net.named_data.jndn.Name
import java.util.logging.Logger

/**
 * NVDR Interest class. Follow the scheme as described in the thesis.
 *
 * @param registryPrefix The name space of the registry ndn node ,e.g., "/nur"
 * @param nameSpace The assigned name space. Important for the registration and data management Interests.
 * @param commandType The NVDR Command, e.g. "insert"
 * @param parameter Information for the Interest, e.g. [CommandParameter] as url encoded string
 * @param suffix Additional name which does not belong to the previous parameter
 */
class RegistryCommandInterest(
    registryPrefix: Name,
    nameSpace: Name,
    val commandType: CommandType,
    // make sure that the parameter is always a url decoded string
    val parameter: URLDecodedString? = null,
    val suffix: Name? = null
) : Interest(
    Name(registryPrefix).append(nameSpace).append(commandType.toString()).apply {
        if (parameter != null) append(Name(URLEncodedString(parameter.string).string))
        if (suffix != null) append(suffix)
    }
) {
    val simpleName = Name(registryPrefix) + nameSpace + Name(commandType.toString())

    init {
        setDefaultCanBePrefix(true)
    }

    constructor(registryPrefix: Name, commandType: CommandType, interest: Interest) : this(
        // TODO handle wrong prefix
        registryPrefix,
        getNameSpace(registryPrefix, commandType, interest.name),
        commandType,
        getAdditionalParameter(
            registryPrefix,
            getNameSpace(registryPrefix, commandType, interest.name),
            commandType,
            interest.name
        ),
        getSuffix(
            registryPrefix,
            getNameSpace(registryPrefix, commandType, interest.name),
            commandType,
            interest.name
        )
    )

    constructor(
        registryPrefix: Name,
        nameSpace: Name,
        commandType: CommandType,
        parameter: InterestParameter,
    ) : this(
        registryPrefix,
        nameSpace,
        commandType,
        URLDecodedString(Json.encodeToString(parameter))
    )

    /**
     * Try to decode the interest's name parameter to a pre-defined object
     */
    inline fun <reified T : InterestParameter> decodeParameter(): T? = try {
        val urlDecodedParameter = parameter?.string ?: ""
        Json.decodeFromString<InterestParameter>(urlDecodedParameter) as T
    } catch (e: Exception) {
        Logger.getLogger(this::class.java.simpleName)
            .warning("Could not decode parameter. See ${e.message}")
        null
    }


    companion object {

        /**
         * @return the parameter for the command (if any given)
         */
        private fun getAdditionalParameter(
            registryPrefix: Name,
            nameSpace: Name,
            commandType: CommandType,
            interestName: Name,
        ): URLDecodedString? {
            // should have registry name space as prefix, e.g. /nur/faber/INSERT/1fesar/1e
            if (!registryPrefix.isPrefixOf(interestName)) return null
            // get the name without the registry prefix e.g. /INSERT/1fesar/1e
            val withoutPrefix = interestName - registryPrefix
            // check if the name prefix is correct
            if (!nameSpace.isPrefixOf(withoutPrefix)) return null
            val withoutNameSpace = withoutPrefix - nameSpace
            val commandTypeName = Name(commandType.toString())
            // check if the command type is correct
            if (!commandTypeName.isPrefixOf(withoutNameSpace)) return null
            // return first component after command, e.g. 1fesar
            val parameter = (withoutNameSpace - commandTypeName).components().firstOrNull()
            return parameter?.run(::URLDecodedString)
        }

        /**
         * @return suffix of the Interest name. The suffix could contain a certificate name for the deregister process.
         */
        private fun getSuffix(
            registryPrefix: Name,
            nameSpace: Name,
            commandType: CommandType,
            interestName: Name,
        ): Name? {
            // should have registry name space as prefix, e.g. /nur/INSERT/1fesar/1e
            if (!registryPrefix.isPrefixOf(interestName)) return null
            // get the name without the registry prefix e.g. /INSERT/1fesar/1e
            val withoutPrefix = interestName - registryPrefix

            // check if the name prefix is correct
            if (!nameSpace.isPrefixOf(withoutPrefix)) return null
            val withoutNameSpace = withoutPrefix - nameSpace

            val commandTypeName = Name(commandType.toString())
            // check if the command type is correct
            if (!commandTypeName.isPrefixOf(withoutNameSpace)) return null
            // get first component after command, e.g. 1fesar
            val command =
                (withoutNameSpace - commandTypeName).components().firstOrNull() ?: return null
            // now get signature
            return (withoutNameSpace - commandTypeName) - Name(command)
        }

        /**
         * @return the assigned name space obtained from the interest name
         */
        private fun getNameSpace(
            registryPrefix: Name,
            commandType: CommandType,
            interestName: Name
        ): Name {
            // should have registry name space as prefix, e.g. /nur/faber/INSERT/1fesar/1e
            if (!registryPrefix.isPrefixOf(interestName)) return Name()
            // get the name without the registry prefix e.g. /faber/INSERT/1fesar/1e
            val withoutPrefix = interestName - registryPrefix
            var nameSpace = Name()
            // everything between prefix and command is name space
            for (component in withoutPrefix.components()) {
                if (component.equals(commandType.toString(), true)) break
                nameSpace = nameSpace.append(component)
            }
            return nameSpace
        }


        /**
         * Decodes the command of the given interest
         *
         * @return the command or null if there is not a valid command in the interest name
         */
        fun decodeCommand(ndnRepoPrefix: Name, interest: Interest) =
            // the first part after the ndn repo prefix has to be the command
            interest.name.components().filter { component ->
                !ndnRepoPrefix.components().contains(component)
            }.mapNotNull { component ->
                try {
                    CommandType.valueOf(component)
                } catch (e: Exception) {
                    null
                }
            }.firstOrNull()
    }
}

/**
 * Easy way to create PROBE NVDR Interest
 */
fun ProbeInterest(registryPrefix: Name, parameter: ProbeParameter) =
    RegistryCommandInterest(registryPrefix, Name(), CommandType.PROBE, parameter)

/**
 * Easy way to create NEW NVDR Interest
 */
fun NewInterest(registryPrefix: Name, parameter: NewParameter) =
    RegistryCommandInterest(registryPrefix, Name(), CommandType.NEW, parameter)

/**
 * Easy way to create SELECT NVDR Interest
 */
fun SelectInterest(registryPrefix: Name, nameSpace: Name, parameter: SelectParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace,CommandType.SELECT, parameter)

/**
 * Easy way to create VALIDATE NVDR Interest
 */
fun ValidateInterest(registryPrefix: Name,nameSpace: Name,  parameter: ValidateParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace, CommandType.VALIDATE, parameter)

/**
 * Easy way to create DOWNLOAD NVDR Interest
 */
fun DownloadInterest(registryPrefix: Name, nameSpace: Name, parameter: DownloadParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace,CommandType.DOWNLOAD, parameter)

/**
 * Easy way to create INSERT_CHECK NVDR Interest
 */
fun InsertCheckInterest(registryPrefix: Name, nameSpace: Name, parameter: CommandParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace, CommandType.INSERT_CHECK, parameter)

/**
 * Easy way to create DELETE_CHECK NVDR Interest
 */
fun DeleteCheckInterest(registryPrefix: Name,nameSpace: Name, parameter: CommandParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace, CommandType.DELETE_CHECK, parameter)

/**
 * Easy way to create INSERT NVDR Interest
 */
fun InsertInterest(registryPrefix: Name, nameSpace: Name, parameter: CommandParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace, CommandType.INSERT, parameter)

/**
 * Easy way to create DELETE NVDR Interest
 */
fun DeleteInterest(registryPrefix: Name,nameSpace: Name, parameter: CommandParameter) =
    RegistryCommandInterest(registryPrefix, nameSpace, CommandType.DELETE, parameter)


/**
 * Easy way to create DE-REGISTER NVDR Interest
 */
fun DeRegisterInterest(registryPrefix: Name, certificateName: Name) =
    RegistryCommandInterest(registryPrefix, Name(), CommandType.DEREGISTER, suffix = certificateName)