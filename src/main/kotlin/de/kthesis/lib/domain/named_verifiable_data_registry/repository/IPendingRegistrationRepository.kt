package de.kthesis.lib.domain.named_verifiable_data_registry.repository

import net.named_data.jndn.Name

/**
 * key is the request id. Contains information for the registration process.
 */
interface IPendingRegistrationRepository {

    /**
     * @return true if the name space is already part of a registration process.
     */
    fun hasNameSpace(nameSpace : Name) :  Boolean

    fun add(value: CertificateRepositoryEntry): String

    fun find(key : String) : CertificateRepositoryEntry?

    fun update(requestId:String, entry : CertificateRepositoryEntry) : Boolean
    fun remove(certificateName: Name)
    fun findAll(): List<CertificateRepositoryEntry>
}