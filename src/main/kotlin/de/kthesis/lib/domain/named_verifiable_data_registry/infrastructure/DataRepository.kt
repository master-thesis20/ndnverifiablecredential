package de.kthesis.lib.domain.named_verifiable_data_registry.infrastructure

import de.kthesis.lib.domain.named_verifiable_data_registry.repository.IDataRepository
import net.named_data.jndn.Data
import net.named_data.jndn.Name

/**
 * Runtime repository to store data packets. Does not store any information persistent.
 *
 * Just for demo purposes.
 */
class DataRepository : Repository<Name, Data>(), IDataRepository {

    override fun removeByPrefix(prefix: Name) {
        val namesWithPrefix = runtimeStorage
            .filter { entry -> prefix.isPrefixOf(entry.key) }.keys
        namesWithPrefix.forEach(::remove)
    }

    override fun findFirstByPrefix(prefix: Name): Data? =
        runtimeStorage
            .filter { entry -> prefix.isPrefixOf(entry.key) }
            .values.firstOrNull()


    override fun add(value: Data): Boolean = _add(value.name, value)

    override fun update(value: Data): Boolean = _update(value.name, value)

    override fun findAll() =
        runtimeStorage.values
}