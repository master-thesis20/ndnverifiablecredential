package de.kthesis.lib.domain.named_verifiable_object

import foundation.identity.jsonld.JsonLDObject
import foundation.identity.jsonld.JsonLDUtils

/**
 * Plain old verifiable credential ()can be used for [NamedVerifiablePresentation])
 */
class VerifiableCredential(
    credentialMetaData: CredentialMetaData,
    vararg credentialSubject: Map<String, Any>
) : JsonLDObject(credentialMetaData) {

    init {
        // credential subject is either available as list or as single object
        if (credentialSubject.size == 1)
            JsonLDUtils.jsonLdAdd(this, CREDENTIAL_SUBJECT_JSON_KEY, credentialMetaData)
        else
            JsonLDUtils.jsonLdAddAsJsonArray(
                this,
                CREDENTIAL_SUBJECT_JSON_KEY,
                credentialSubject
            )
    }


}