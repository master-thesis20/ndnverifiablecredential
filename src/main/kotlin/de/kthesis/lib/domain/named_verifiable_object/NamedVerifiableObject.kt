package de.kthesis.lib.domain.named_verifiable_object

import de.kthesis.lib.domain.named_verifiable_object.interfaces.IEncryption
import foundation.identity.jsonld.JsonLDObject
import net.named_data.jndn.Data
import net.named_data.jndn.Name
import net.named_data.jndn.util.Blob
import org.json.JSONObject


/**
 * Base class for Verifiable Credential/Presentation as Named Network data data.
 * Contains the mandatory base properties, which are necessary for verifiable objects as
 * mentioned in the standard recommendation
 * https://www.w3.org/TR/2022/REC-vc-data-model-20220303/
 *
 * @param name The name of the verifiable object
 * @param content The json-ld object, which contains the meta-data and the corresponding content (e.g. credential subject)
 */
sealed class NamedVerifiableObject(
    name: Name,
    content: JsonLDObject
) : Data(name) {

    override fun toString(): String = content.toString()

    init {
        // simply encode the content as string
        this.content = Blob(content.toJson())
    }

    /**
     * Add a proof object according to the associated certificate.
     */
    fun addProof() {
        // TODO add proof object to content
        val jsonLDObject = JsonLDObject.fromJson(content.toString())
        // update content
        this.content = Blob(jsonLDObject.toJson())
    }

    /**
     * Encrypt the content of the data packet
     */
    fun encrypt(encryption : IEncryption) {
        content = Blob(encryption.encrypt(content.immutableArray))
    }

    /**
     * Returns the content of a field of the verifiable object content.
     *
     * @param key The key of the field, e.g. "issuer"
     * @return The json field value or null if its not available or not set
     */
    inline operator fun <reified T : Any>get(key: String): T? {
        val json = JSONObject(content.toString())
        if (!json.has(key)) return null
        val value = json.get(key)
        return value as? T
    }

}





