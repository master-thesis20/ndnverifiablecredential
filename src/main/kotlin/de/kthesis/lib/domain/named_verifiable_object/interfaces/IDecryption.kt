package de.kthesis.lib.domain.named_verifiable_object.interfaces

fun interface IDecryption {

    /**
     * Base function to decrypt data packet content.
     *
     * @param The content of a data packet
     * @return The decrypted content
     */
    fun decrypt(encryptedContent: ByteArray): ByteArray
}