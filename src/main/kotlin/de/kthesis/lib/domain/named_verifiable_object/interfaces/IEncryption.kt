package de.kthesis.lib.domain.named_verifiable_object.interfaces

fun interface IEncryption {
    /**
     * Base function to encrypt the data packet content.
     *
     * @param plainContent The not-encrypted content of a data packet
     *
     * @return The encrypted content
     */
    fun encrypt(plainContent : ByteArray) : ByteArray
}