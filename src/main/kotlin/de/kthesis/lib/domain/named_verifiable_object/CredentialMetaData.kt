package de.kthesis.lib.domain.named_verifiable_object

import java.net.URI
import java.util.Date


/**
 * Contains meta information for a verifiable credential, such as Issuer, etc.
 *
 * credential meta-data has to have at least one Issuer and an Issuance Date.
 */
class CredentialMetaData private constructor(
    context: List<URI>,
    type: List<URI>,
    issuanceDate: Date,
    vararg additionalProperties: Pair<String, Any>
) : LinkedHashMap<String, Any>(
    mapOf(
        *additionalProperties,
        ISSUANCE_PROPERTY_JSON_KEY to issuanceDate,
        CONTEXT_PROPERTY_JSON_KEY to context,
        TYPE_PROPERTY_JSON_KEY to type,
    )
) {

    /**
     * Contains meta information for a verifiable credential, such as Issuer, etc.
     *
     * credential meta-data has to have at least one Issuer and an Issuance Date, other properties are optinal.
     *
     * @param context The list of context URIs which are necessary to interpret the fields of the credential
     * @param type The type is necessary to resolve the credential with the help of the context urls. E.g. "VerifiableCredential"
     * @param issuer The Issuer could be specified as simple URI conform identifier , e.g. "did:issuer:123"
     * @param issuanceDate The timestamp which marks the validation begin of the credential
     * @param additionalProperties Additional meta data information, e.g. "expiresAt"
     */
    constructor(
        context: List<URI>,
        type: List<URI>,
        issuer: URI,
        issuanceDate: Date,
        vararg additionalProperties: Pair<String, Any>
    ) : this(
        context,
        type,
        issuanceDate,
        *listOf(*additionalProperties, Pair(ISSUER_PROPERTY_JSON_KEY, issuer)).toTypedArray()
    )

    /**
     * Contains meta information for a verifiable credential, such as Issuer, etc.
     *
     * credential meta-data has to have at least one Issuer and an Issuance Date, other properties are optinal.
     *
     * @param context The list of context URIs which are necessary to interpret the fields of the credential
     * @param type The type is necessary to resolve the credential with the help of the context urls. E.g. "VerifiableCredential"
     * @param issuer The Issuer given as komplex type. See [Issuer] for a detailed explanation.
     * @param issuanceDate The timestamp which marks the validation begin of the credential
     * @param additionalProperties Additional meta data information, e.g. "expiresAt"
     */
    constructor(
        context: List<URI>,
        type: List<URI>,
        issuer: Issuer,
        issuanceDate: Date,
        vararg additionalProperties: Pair<String, Any>
    ) : this(
        context,
        type,
        issuanceDate,
        *listOf(*additionalProperties, Pair(ISSUER_PROPERTY_JSON_KEY, issuer)).toTypedArray()
    )


}