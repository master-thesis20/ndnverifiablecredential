package de.kthesis.lib.domain.named_verifiable_object

import de.kthesis.lib.domain.named_verifiable_object.interfaces.IDecryption
import de.kthesis.lib.infrastructure.utils.toArr
import foundation.identity.jsonld.JsonLDObject
import foundation.identity.jsonld.JsonLDUtils
import net.named_data.jndn.Data
import net.named_data.jndn.Name
import net.named_data.jndn.util.Common
import org.json.JSONArray

class NamedVerifiablePresentation private constructor(
    name: Name, jsonLDObject: JsonLDObject
) : NamedVerifiableObject(
    name = name, content = jsonLDObject
) {

    /**
     * Create a ndn-native verifiable presentation.
     * Follows the convention of the W3C recommendation.
     * See https://www.w3.org/TR/2022/REC-vc-data-model-20220303/
     *
     * @param name The name of the verifiable presentation
     * @param presentationMetaData The meta-data of the presentation. See [PresentationMetaData] for more information.
     * @param verifiableCredentials One or more verifiable credentials.
     */
    constructor(
        name: Name,
        presentationMetaData: PresentationMetaData,
        vararg verifiableCredentials: JsonLDObject
    ) : this(name, JsonLDObject.fromMap(presentationMetaData).apply {
        // credential subject is either available as list or as single object
        if (verifiableCredentials.size == 1) JsonLDUtils.jsonLdAdd(
            this,
            VERIFIABLE_CREDENTIAL_JSON_KEY,
            verifiableCredentials
        )
        else JsonLDUtils.jsonLdAddAsJsonArray(
            this, VERIFIABLE_CREDENTIAL_JSON_KEY, verifiableCredentials
        )
    })

    /**
     * Create a ndn-native verifiable presentation.
     * Follows the convention of the W3C recommendation.
     * See https://www.w3.org/TR/2022/REC-vc-data-model-20220303/
     *
     * @param name The name of the verifiable presentation
     * @param presentationMetaData The meta-data of the presentation. See [PresentationMetaData] for more information.
     * @param verifiableCredentials One or more verifiable credentials.
     */
    constructor(
        name: Name,
        presentationMetaData: PresentationMetaData,
        vararg verifiableCredentials: Map<String, Any>
    ) : this(
        name,
        presentationMetaData,
        *verifiableCredentials.map { JsonLDObject.fromMap(it) }.toTypedArray()
    )

    /**
     * Create a ndn-native verifiable presentation.
     * Follows the convention of the W3C recommendation.
     * See https://www.w3.org/TR/2022/REC-vc-data-model-20220303/
     *
     * @param name The name of the verifiable presentation
     * @param presentationMetaData The meta-data of the presentation. See [PresentationMetaData] for more information.
     * @param verifiableCredentials One or more [NamedVerifiableCredential].
     */
    constructor(
        name: Name,
        presentationMetaData: PresentationMetaData,
        vararg namedVerifiableCredential: NamedVerifiableCredential
    ) : this(
        name, presentationMetaData, *namedVerifiableCredential.map { credential ->
            // encode the data to string and add it to the json object
            val encodedNVC = Common.base64Encode(credential.wireEncode().buf().toArr())
            JsonLDObject.fromMap(
                mapOf(
                    NAMED_VERIFIABLE_CREDENTIAL_JSON_KEY to encodedNVC
                )
            )
        }.toTypedArray()
    )

    fun interface TypeCheck {

        /**
         * Implement a custom checker to determine if the data packet is a [NamedVerifiablePresentation].
         * The data packet is decrypted.
         *
         * @return true if the data is a [NamedVerifiablePresentation], false otherwise.
         */
        fun isNamedVerifiablePresentation(data: Data): Boolean
    }

    /**
     * Convert the given Data to a NamedVerifiablePresentation. Add a type check implementation to assure
     * that the data actual contains a presentation.
     * If the Data is encrypted, pass a suitable decryptor.
     *
     * @param data The data which should contain a [NamedVerifiablePresentation]
     * @param typeCheck Check if the data contains a presentation
     * @param decryption If the data is encrypted, pass decryption functionality in order to decrypt the data packet
     * (else the presentation cannot be constructed)
     */
    constructor(
        data: Data,
        typeCheck: TypeCheck,
        decryption: IDecryption?
    // decrypt the content first
    ) : this(data.name, decryption?.decrypt(data.content.immutableArray)?.run {
        JsonLDObject.fromJson(String(this))
    } ?: JsonLDObject.fromJson(data.content.toString())) {
        // assure after creation that the data is a presentation
        require(typeCheck.isNamedVerifiablePresentation(this)) {
            "$name is not a valid named verifiable presentation data packet"
        }
        metaInfo = data.metaInfo
        signature = data.signature
    }

    /**
     * If the presentation contains an encoded [NamedVerifiableCredential], return them.
     *
     * @param typeCheck Pass a checker to determine if the encoded data packet is a named verifiable credential.
     * @param decryption If the credential is encrypted, pass a decryption function in order to obtain the credential.
     *
     * @return A list with the obtained verifiable credentials. Return empty list if no credentials could be found
     */
    fun getNamedVerifiableCredentials(
        typeCheck: NamedVerifiableCredential.TypeCheck,
        decryption: IDecryption?
    ): List<NamedVerifiableCredential> {
        val verifiableCredential =
            get<JSONArray>(VERIFIABLE_CREDENTIAL_JSON_KEY) ?: return emptyList()
        return verifiableCredential
            .map { JsonLDObject.fromJson(it.toString()) }
            .mapNotNull { jsonLDObject ->
            val encodedNVC =
                jsonLDObject.jsonObject[NAMED_VERIFIABLE_CREDENTIAL_JSON_KEY]?.toString()
                    ?: return@mapNotNull null
            NamedVerifiableCredential(encodedNVC, typeCheck, decryption)
        }
    }

}