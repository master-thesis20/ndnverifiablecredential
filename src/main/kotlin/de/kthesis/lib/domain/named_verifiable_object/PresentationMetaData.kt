package de.kthesis.lib.domain.named_verifiable_object

import java.net.URI

/**
 * Represents the meta information for a verifiable presentation.
 *
 * See https://www.w3.org/TR/2022/REC-vc-data-model-20220303/#presentations-0
 * for more information.
 *
 * @param context Contains a list of URIs which define the fields of the presentation to make it interpretable.
 * @param type Each Verifiable Presentation has to have at least one type which can be used to determine the fields,
 * combines with the context uris. E.g. "VerifiablePresentation"
 * @param additionalProperties Contains more information about the verifiable presentation, e.g. a "expiresAt" field.
 */
class PresentationMetaData(
    context: List<URI>,
    type: List<URI>,
    vararg additionalProperties: Pair<String, Any>
) : LinkedHashMap<String, Any>(
    mapOf(
        *additionalProperties,
        CONTEXT_PROPERTY_JSON_KEY to context,
        TYPE_PROPERTY_JSON_KEY to type,
    )
)