package de.kthesis.lib.domain.named_verifiable_object

import foundation.identity.jsonld.JsonLDObject
import java.net.URI

/**
 * Create Issuer object, which has to have a mandatory identifier
 *
 * Refers to the 'issuer' property as defined in https://www.w3.org/TR/2022/REC-vc-data-model-20220303/#issuer
 *
 * @param id A URI conform identifier for the Issuer
 * @param additionalInformation Additional information for the issuer, e.g. a human friendly name
 */
class Issuer(id: URI, additionalInformation: Map<String, Any>) : JsonLDObject(
    additionalInformation.toMutableMap().apply { put(IDENTIFIER_PROPERTY_JSON_KEY, id) }
)