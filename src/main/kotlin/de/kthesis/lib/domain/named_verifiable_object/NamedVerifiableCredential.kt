package de.kthesis.lib.domain.named_verifiable_object

import de.kthesis.lib.domain.named_verifiable_object.interfaces.IDecryption
import foundation.identity.jsonld.JsonLDObject
import foundation.identity.jsonld.JsonLDUtils
import net.named_data.jndn.Data
import net.named_data.jndn.Name
import net.named_data.jndn.util.Blob
import net.named_data.jndn.util.Common

/**
 * Represents a ndn-native verifiable credential.
 */
class NamedVerifiableCredential private constructor(
    name: Name,
    jsonLDObject: JsonLDObject
) : NamedVerifiableObject(
    name = name,
    content = jsonLDObject
) {

    /**
     * Create a ndn-native verifiable credential.
     * Follows the convention of the W3C recommendation.
     * See https://www.w3.org/TR/2022/REC-vc-data-model-20220303/
     *
     * @param name The name of the verifiable credential
     * @param credentialMetaData The meta-data of the credential. See [CredentialMetaData] for more information.
     * @param credentialSubject One or more credentials subjects.
     */
    constructor(
        name: Name,
        credentialMetaData: CredentialMetaData,
        vararg credentialSubject: Map<String, Any>
    ) : this(
        name, JsonLDObject.fromMap(credentialMetaData).apply {
            // credential subject is either available as list or as single object
            if (credentialSubject.size == 1)
                JsonLDUtils.jsonLdAdd(this, CREDENTIAL_SUBJECT_JSON_KEY, credentialSubject)
            else
                JsonLDUtils.jsonLdAddAsJsonArray(
                    this,
                    CREDENTIAL_SUBJECT_JSON_KEY,
                    credentialSubject
                )
        }
    )

    fun interface TypeCheck {
        /**
         * Implement a custom checker to determine if the data packet is a [NamedVerifiableCredential].
         * The data packet is decrypted.
         *
         * @return true if the data is a [NamedVerifiableCredential], false otherwise.
         */
        fun isNamedVerifiableCredential(data: Data): Boolean
    }

    /**
     * Convert the given Data to a NamedVerifiableCredential. Add a type check implementation to assure
     * that the data actual contains a credential.
     * If the Data is encrypted, pass a suitable decryptor.
     *
     * @param data The data which should contain a [NamedVerifiableCredential]
     * @param typeCheck Check if the data contains a credential
     * @param decryption If the data is encrypted, pass decryption functionality in order to decrypt the data packet
     * (else the credential cannot be constructed)
     */
    constructor(
        data: Data,
        typeCheck: TypeCheck,
        decryption: IDecryption?
    ) : this(
        data.name,
        decryption?.decrypt(data.content.immutableArray)?.run {
            JsonLDObject.fromJson(String(this))
        } ?: JsonLDObject.fromJson(data.content.toString())
    ) {
        require(typeCheck.isNamedVerifiableCredential(this)) { "$name is not a valid named verifiable credential data packet" }
        metaInfo = data.metaInfo
        signature = data.signature
    }

    /**
     * Decode the given string to a [NamedVerifiableCredential] object.
     * Mostly used when a [NamedVerifiableCredential] was included in a [NamedVerifiablePresentation].
     *
     * @param encodedPacket The encoded [NamedVerifiableCredential] (could be an encoded [Data] packet,too)
     * @param typeCheck Check if the encoded Data is actual a credential
     * @param decryption If the decoded data is encrypted, pass decryption functionality in order to decrypt the decoded data packet
     * (else the credential cannot be created)
     */
    constructor(
        encodedPacket: String,
        typeCheck: TypeCheck,
        decryption: IDecryption?
    ) : this(
        Data().apply { wireDecode(Blob(Common.base64Decode(encodedPacket))) }, typeCheck, decryption
    )


}