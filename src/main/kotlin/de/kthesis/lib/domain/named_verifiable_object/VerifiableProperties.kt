package de.kthesis.lib.domain.named_verifiable_object

const val CONTEXT_PROPERTY_JSON_KEY = "@context"
const val TYPE_PROPERTY_JSON_KEY = "type"
const val ISSUER_PROPERTY_JSON_KEY = "issuer"
const val IDENTIFIER_PROPERTY_JSON_KEY = "issuer"
const val ISSUANCE_PROPERTY_JSON_KEY = "issuanceDate"
const val EXPIRATION_PROPERTY_JSON_KEY = "expirationDate"
const val CREDENTIAL_SUBJECT_JSON_KEY = "credentialSubject"
const val VERIFIABLE_CREDENTIAL_JSON_KEY = "verifiableCredential"

/**
 *  json key for an encoded named verifiable credential data packet
 */
const val NAMED_VERIFIABLE_CREDENTIAL_JSON_KEY = "encodedNamedVerifiableCredential"