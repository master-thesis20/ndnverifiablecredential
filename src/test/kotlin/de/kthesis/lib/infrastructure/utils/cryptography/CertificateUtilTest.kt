package de.kthesis.lib.infrastructure.utils.cryptography

import net.named_data.jndn.Interest
import net.named_data.jndn.Name
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.RsaKeyParams
import net.named_data.jndn.security.certificate.PublicKey
import org.junit.jupiter.api.Test

internal class CertificateUtilTest {

    @Test
    fun testCreateSignedInterest()  {
        /* setup necessary test objects/mocks for feature test*/
        val testIdentity = Name("test")
        val testKeyChain = KeyChain("pib-memory:", "tpm-memory:").apply {
            createIdentityV2(testIdentity, RsaKeyParams(4096))
        }
        val testInterest = Interest(Name("test/interest"))

        val testCertificate = CertificateUtil.createCertificateV2(
            Name("test/another-test"),
            testKeyChain.pib.defaultIdentity.defaultKey.name,
            PublicKey(testKeyChain.pib.defaultIdentity.defaultKey.publicKey),
            testKeyChain
        )

        /* feature to be tested */
        /* create a signed interest */

        val actualInterest = CertificateUtil.sign(
            keyChain = testKeyChain,
            interest = testInterest,
            signingKeyName = testKeyChain.pib.defaultIdentity.defaultKey.name,
            keylocatorKeyName = testCertificate.name,
        )

        testKeyChain.verifyInterest(actualInterest, { it ->
            assert(true)
            println(it.name)
        }, { it, error ->
            assert(false)
        })
    }
}