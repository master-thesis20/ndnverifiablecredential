package de.kthesis.lib.domain.named_verifiable_object

import net.named_data.jndn.Data
import net.named_data.jndn.KeyLocator
import net.named_data.jndn.Name
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.RsaKeyParams
import org.junit.jupiter.api.Test
import java.net.URI
import java.util.Date
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class NamedVerifiablePresentationTest {

    @Test
    fun testNamedVerifiableCredentialRetrieval() {
        val expectedCredential = NamedVerifiableCredential(
            Name("faber/credential/alice"), CredentialMetaData(
                context = listOf(URI("https://www.w3.org/2018/credentials/v1")),
                type = listOf(URI("BachelorDegree")),
                issuanceDate = Date(),
                issuer = Issuer(URI("FaberUniversity"), emptyMap())
            ), mapOf(
                "name" to "Alice",
                "grade" to "1.7"
            )
        )

        val testKeyChain = KeyChain("pib-memory:", "tpm-memory:").apply {
            createIdentityV2(Name("bla"), RsaKeyParams(4096))
        }
        testKeyChain.sign(expectedCredential,testKeyChain.defaultCertificateName)

        val presentation = NamedVerifiablePresentation(
            name = Name(),
            presentationMetaData = PresentationMetaData(
                context = listOf(URI("https://www.w3.org/2018/credentials/v1")),
                type = listOf(URI("BachelorDegreePresentation")),
            ),
            // include the verifiable credential data packet
            expectedCredential
        )

        val actualCredential = presentation.getNamedVerifiableCredentials({true},null).firstOrNull()

        // make sure the credential is the same
        assertNotNull(actualCredential)
        assertEquals(expectedCredential.name,actualCredential.name)
        assertEquals(expectedCredential.content.toString(), actualCredential.content.toString())
        assertEquals(KeyLocator.getFromSignature(expectedCredential.signature).keyName,KeyLocator.getFromSignature(actualCredential.signature).keyName)
        assertEquals(expectedCredential.signature.signature,actualCredential.signature.signature)
    }

    @Test
    fun testNamedVerifiableCredentialEncoding() {
        val expectedCredential = NamedVerifiableCredential(
            Name("faber/credential/alice"), CredentialMetaData(
                context = listOf(URI("https://www.w3.org/2018/credentials/v1")),
                type = listOf(URI("BachelorDegree")),
                issuanceDate = Date(),
                issuer = Issuer(URI("FaberUniversity"), emptyMap())
            ), mapOf(
                "name" to "Alice",
                "grade" to "1.7"
            )
        )

        val testKeyChain = KeyChain("pib-memory:", "tpm-memory:").apply {
            createIdentityV2(Name("bla"), RsaKeyParams(4096))
        }


        testKeyChain.sign(expectedCredential,testKeyChain.defaultCertificateName)

        val data = Data(expectedCredential)
        val actualCredential = NamedVerifiableCredential(data,{true},null)
        assertNotNull(actualCredential)
        assertEquals(expectedCredential.name,actualCredential.name)
        assertEquals(expectedCredential.content.toString(), actualCredential.content.toString())
        assertEquals(KeyLocator.getFromSignature(expectedCredential.signature).keyName,KeyLocator.getFromSignature(actualCredential.signature).keyName)
        assertEquals(expectedCredential.signature.signature,actualCredential.signature.signature)
    }

}