package de.kthesis.lib.domain.named_verifiable_data_registry

import de.kthesis.lib.domain.named_verifiable_data_registry.command.CommandType
import de.kthesis.lib.domain.named_verifiable_data_registry.command.RegistryCommandInterest
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.DownloadParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.NewParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ProbeParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.SelectParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ValidateParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ChallengeResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.CommandResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.DownloadResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.NewResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.ProbeResponse
import de.kthesis.lib.domain.named_verifiable_data_registry.response.RegistryCommandResponseData
import de.kthesis.lib.domain.named_verifiable_data_registry.response.StatusCode
import de.kthesis.lib.infrastructure.utils.cryptography.CertificateUtil
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import net.named_data.jndn.Data
import net.named_data.jndn.Face
import net.named_data.jndn.Interest
import net.named_data.jndn.Name
import net.named_data.jndn.OnData
import net.named_data.jndn.OnTimeout
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.RsaKeyParams
import net.named_data.jndn.security.v2.CertificateV2
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

/**
 * Not a unit test more a integration test
 */
@OptIn(ExperimentalCoroutinesApi::class)
internal class NamedVerifiableDataRegistryTest {

    lateinit var namedVerifiableDataRegistry: NamedVerifiableDataRegistry

    @RelaxedMockK
    lateinit var mockedFace: Face

    private val registryPrefix = Name("/nur")

    private val registryKeyChain = KeyChain("pib-memory:", "tpm-memory:").apply {
        createIdentityV2(registryPrefix, RsaKeyParams(4096))
    }

    private val testPrefix = Name("/test")

    private val testKeyChain = KeyChain("pib-memory:", "tpm-memory:").apply {
        createIdentityV2(testPrefix, RsaKeyParams(4096))
    }


    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
        namedVerifiableDataRegistry = NamedVerifiableDataRegistry(
            nvdrPrefix = registryPrefix,
            face = mockedFace,
            keyChain = registryKeyChain,
            rootCertificateList = listOf(registryKeyChain.pib.defaultIdentity.defaultKey.defaultCertificate),
            mockk()
        )
    }

    @Test
    fun testSampleProcedure() = runTest {
        val expectedCertificateName = Name(registryPrefix).append(Name(testPrefix))

        val probeInterest = RegistryCommandInterest(
            registryPrefix, Name(), CommandType.PROBE, ProbeParameter(desiredNameSpace = testPrefix)
        )

        var requestId = ""
        var registeredCertificate = CertificateV2()

        val slot = slot<RegistryCommandResponseData>()
        every {
            mockedFace.putData(capture(slot))
        } answers {
            val probeResponse = slot.captured.decodeParameter<ProbeResponse>()

            assertNotNull(probeResponse)
            // response contains name space
            assertEquals(testPrefix.toString(), probeResponse.identifier)
            // safe requestId
            requestId = probeResponse.requestId
        } andThenAnswer {
            val newResponse = slot.captured.decodeParameter<NewResponse>()

            assertNotNull(newResponse)
            assertEquals(requestId, newResponse.requestId)
        } andThenAnswer {
            val selectResponse = slot.captured.decodeParameter<ChallengeResponse>()

            assertNotNull(selectResponse)
            assertEquals(requestId, selectResponse.requestId)
            assertNull(selectResponse.certificateName)
        } andThenAnswer {
            val validateResponse = slot.captured.decodeParameter<ChallengeResponse>()

            assertNotNull(validateResponse)
            assertEquals(requestId, validateResponse.requestId)
            assert(expectedCertificateName.isPrefixOf(validateResponse.certificateName))
        } andThenAnswer {
            val downloadResponse = slot.captured.decodeParameter<DownloadResponse>()

            assertNotNull(downloadResponse)
            assertNotNull(downloadResponse.certificateV2)
            assert(expectedCertificateName.isPrefixOf(downloadResponse.certificateV2!!.name))
            registeredCertificate = downloadResponse.certificateV2!!
        }

        // start with PROBE Interest
        namedVerifiableDataRegistry.onInterest(
            registryPrefix, probeInterest, mockedFace, 0, null
        )

        val certificateRequest = testKeyChain.pib.defaultIdentity.defaultKey.defaultCertificate
        val newInterest = RegistryCommandInterest(
            registryPrefix,Name(), CommandType.NEW, NewParameter(requestId = requestId, certificateRequest)
        )

        // send a NEW request
        namedVerifiableDataRegistry.onInterest(
            registryPrefix, newInterest, mockedFace, 0, null
        )

        val selectRequest = RegistryCommandInterest(
            registryPrefix, Name(),CommandType.SELECT, SelectParameter(requestId = requestId, challenge = "unused")
        )

        // send a challenge SELECT interest
        namedVerifiableDataRegistry.onInterest(
            registryPrefix, selectRequest, mockedFace, 0, null
        )

        val validateInterest = RegistryCommandInterest(
            registryPrefix, Name(), CommandType.VALIDATE, ValidateParameter(requestId = requestId, challenge = "still unused")
        )

        // send a challenge VALIDATE interest
        namedVerifiableDataRegistry.onInterest(
            registryPrefix, validateInterest, mockedFace, 0, null
        )

        val downloadInterest = RegistryCommandInterest(
            registryPrefix,Name(), CommandType.DOWNLOAD, DownloadParameter(requestId)
        )

        // send a DOWNLOAD interest to retrieve certificate
        namedVerifiableDataRegistry.onInterest(
            registryPrefix, downloadInterest, mockedFace, 0, null
        )

        // the certificate is registered and data can be inserted
        val testDataName = Name("test/data")
        val insertInterest = RegistryCommandInterest(
            registryPrefix, Name(),CommandType.INSERT, CommandParameter(testDataName)
        )

        val signedInterest = CertificateUtil.sign(
            testKeyChain,
            insertInterest,
            registeredCertificate.name,
            testKeyChain.pib.defaultIdentity.defaultKey.name,
        )

        val requestDataNameSlot = slot<Name>()
        val onDataSlot = slot<OnData>()
        val onTimeoutSlot = slot<OnTimeout>()
        every {
            mockedFace.expressInterest(capture(requestDataNameSlot), capture(onDataSlot), capture(onTimeoutSlot))
        } answers {
            val requestedTestDataName = requestDataNameSlot.captured

            assertNotNull(requestedTestDataName)
            assertTrue(testDataName.isPrefixOf(requestedTestDataName))

            // return test data
            val testData = Data(requestedTestDataName).apply {
                metaInfo.finalBlockId = Name.Component("0")
            }
            onDataSlot.captured.onData(Interest(requestedTestDataName), testData)
            1L
        }

        every {
            mockedFace.putData(capture(slot))
        } answers {
            val insertResponse = slot.captured.decodeParameter<CommandResponse>()

            assertNotNull(insertResponse)
            assertEquals(StatusCode.INSERTION_COMMAND_VALID, insertResponse.statusCode)
        }

        // start data insertion
        namedVerifiableDataRegistry.onInterest(
            registryPrefix, signedInterest, mockedFace, 0, null
        )
    }

    @AfterEach
    fun tearDown() {
        clearAllMocks()
    }
}