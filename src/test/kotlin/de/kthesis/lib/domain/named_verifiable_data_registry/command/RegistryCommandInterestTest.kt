package de.kthesis.lib.domain.named_verifiable_data_registry.command

import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.CommandParameter
import de.kthesis.lib.domain.named_verifiable_data_registry.command.parameter.ProbeParameter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.named_data.jndn.Interest
import net.named_data.jndn.Name
import net.named_data.jndn.security.KeyChain
import net.named_data.jndn.security.RsaKeyParams
import net.named_data.jndn.util.Blob
import org.bouncycastle.util.encoders.Hex
import org.junit.jupiter.api.Test
import java.net.URLDecoder
import java.net.URLEncoder

internal class RegistryCommandInterestTest {


    @Test
    fun testObtainCommandInterestFromSignedInterest()  {
        /* setup necessary test objects/mocks for feature test*/
        val interest = Interest(Name("/test/interest/register")).apply {
            applicationParameters = Blob(Json.encodeToString(CommandParameter(Name())))
        }
        val identifyName = Name("test")
        val keyChain = KeyChain("pib-memory:", "tpm-memory:").apply {
            createIdentityV2(identifyName, RsaKeyParams(4096))
        }
        keyChain.sign(interest)

        /* feature to be tested */
        /* obtain a command interest with an Interest as input */
        //val repoCommandInterest = RepoCommandInterest(Name("/test/interest"),interest)

        val name = Name(Hex.toHexString(".--391=-das d=10 ä".toByteArray()))
        val interest2 = Interest(name)
        println(name)

        val probeParameter = ProbeParameter(
            desiredNameSpace = Name()
        )

        val str = Json.encodeToString(probeParameter)
        val enc = URLEncoder.encode(str, Charsets.UTF_8)
        val result = Json.decodeFromString<ProbeParameter>(URLDecoder.decode(enc,Charsets.UTF_8))
        println(result)
        println(String((Hex.decode(interest2.name.toString().replace("/","")))))
        /* expected behaviour */
        /* command interest as the correct prefix*/
        //assertEquals(Name("/test/interest/register"),repoCommandInterest.name)
    }
}