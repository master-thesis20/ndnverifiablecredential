package de.kthesis.lib.domain.named_verifiable_data_registry.response

import net.named_data.jndn.Name
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class RegistryCommandResponseDataTest {

    @BeforeEach
    fun setUp() {
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun testSerializationOfObject() {
        val registryCommandResponseData = RegistryCommandResponseData(
            Name("cool"),
            CommandResponse(
                statusCode = StatusCode.REGISTRATION_COMMAND_DONE
            )
        )
        val parsed= RegistryCommandResponseData(registryCommandResponseData)

        assertEquals(registryCommandResponseData,parsed)
    }
}