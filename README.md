# NDNVerifiableCredential Library

This project contains the library code for the concepts, as presented in the master thesis
<i>"Synergies between Verifiable Credentials and Information-Centric networks on the example of the
Named Data Networking Project"</i> by Andreas Kalz.

The library is written in kotlin and will be compiled to Java byte code. `gradle` is used as build
tool.

Disclaimer: While the lib implements the basics of the presented concepts, it still far away from a
usage inside production code. The current state is only for testing and as proof of concept for the
thesis.

## Structure

Refer to the thesis for a theoretical explanation of the project structure.

    .
    ├── src/main/kotlin         # Contains the source code as presented in the thesis
    └── build.gradle            # Project build configuration file


## Setup

To use this library in your own projects, include the following configurations, depending on your build tool.
Another possibility would be to build the library and include it as local `.jar` file. 

### Maven

Add the below to your `pom.xml` file.

```xml

<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://git.tu-berlin.de/api/v4/projects/16127/packages/maven</url>
    </repository>
</repositories>

<distributionManagement>
<repository>
    <id>gitlab-maven</id>
    <url>https://git.tu-berlin.de/api/v4/projects/16127/packages/maven</url>
</repository>

<snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://git.tu-berlin.de/api/v4/projects/16127/packages/maven</url>
</snapshotRepository>
</distributionManagement>
```

Copy and paste this inside your `pom.xml` `dependencies` block.

```xml

<dependency>
    <groupId>andreas.thesis</groupId>
    <artifactId>NDNVerifiableCredentials</artifactId>
    <version>1.0.3</version>
</dependency>
```

### Gradle

Add the below to your `build.gradle` in the `repository` block.

```gradle
maven {
  url 'https://git.tu-berlin.de/api/v4/projects/16127/packages/maven'
}
```

Copy and paste this inside your `build.gradle` `dependencies` block.

```gradle
implementation 'andreas.thesis:NDNVerifiableCredentials:1.0.3'
```

